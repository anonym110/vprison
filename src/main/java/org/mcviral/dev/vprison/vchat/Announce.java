package org.mcviral.dev.vprison.vchat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.mcviral.dev.vprison.vPrison;

public class Announce {
	public static Announce announce;
	private vPrison plugin;
	  
	public Announce (vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void broadcast(CommandSender sender, Command cmd, String arg0, String[] args) {
		String msg = "";
		for(int i = 0; i <= args.length - 1; i++) {
			if (!msg.isEmpty())
				msg = msg + " " + args[i];
			else
				msg = args[i];
		}
		plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', msg));
	}
	
	
	
	
}
