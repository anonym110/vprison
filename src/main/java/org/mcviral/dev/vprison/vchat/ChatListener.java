package org.mcviral.dev.vprison.vchat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Utilities;

public class ChatListener {
	public static ChatListener chatlistener;
	private vPrison plugin;
	  
	public ChatListener (vPrison plugin) {
		this.plugin = plugin;
	}

    public void onPlayerJoin(PlayerJoinEvent event){
            Player player = event.getPlayer();
            if(!player.hasPlayedBefore()) {
                plugin.getConfig().set(player.getUniqueId() + ".prestige", "0");
                plugin.getConfig().set(player.getUniqueId() + ".name", player.getName());
            	plugin.saveConfig();
                plugin.getLogger().info("New Player " + player.getName() + " added to Database!");
            }
    }
    
    public void onChat(AsyncPlayerChatEvent event) { 
		Player player = event.getPlayer();
		String format = event.getFormat();      
		String prestige = "";
		
		int rank = plugin.getConfig().getInt(player.getUniqueId() + ".prestige");
		if (rank != 0)
			prestige = " " + Utilities.IntegerToRomanNumeral(rank);
				
		if (prestige == null || prestige.isEmpty()) {
			event.setFormat(format.replace("{PRESTIGE}", ""));   
		} else {   
			event.setFormat(format.replace("{PRESTIGE}", ChatColor.translateAlternateColorCodes('&', prestige )));
		}
	}

}
