package org.mcviral.dev.vprison.vfarmlimit;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcviral.dev.vprison.vPrison;

public class FarmlimitCmd {
	public static FarmlimitCmd farmlimitcmd;
	@SuppressWarnings("unused")
	private vPrison plugin;
	
	public FarmlimitCmd(vPrison plugin) {
		this.plugin = plugin;
	}
 
//	@Override
	public void farmlimit(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
			vFarmlimit.vfarmlimit.checkTimer();
			double farm = 0.0;
			if(vFarmlimit.vfarmlimit.farmlimit.containsKey(player.getName()))
				farm = vFarmlimit.vfarmlimit.farmlimit.get(player.getName());
			player.sendMessage("You have currently used $" + farm + " of your daily limit $" + vFarmlimit.vfarmlimit.limit(player));
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
		}
	}
}