package org.mcviral.dev.vprison.vfarmlimit;

import net.ess3.api.events.SignInteractEvent;

import org.bukkit.entity.Player;
import org.mcviral.dev.vprison.vPrison;

public class FarmlimitListener {
	@SuppressWarnings("unused")
	private vPrison plugin;
	public static FarmlimitListener farmlimitlistener;
	
	public FarmlimitListener(vPrison plugin) {
		this.plugin = plugin;
	}
	
	
    public void onSignInteract(SignInteractEvent event){
            String Type = event.getEssentialsSign().getName();  

            if(Type.equalsIgnoreCase("sell")){
                String Item = event.getSign().getLine(2);
               
            	if(vFarmlimit.vfarmlimit.banned(Item.toLowerCase())) {
            		Player player = event.getUser().getBase();
                    int amount = Integer.parseInt(event.getSign().getLine(1));
                  
	            	if(player.getInventory().containsAtLeast(vFarmlimit.vfarmlimit.Material(Item.toLowerCase()), amount)) {
	                    double price = Double.parseDouble(event.getSign().getLine(3).substring(1));
	            		double oldBal = event.getUser().getMoney().doubleValue();
			            double newBal = oldBal + price;
			            double profit = newBal - oldBal;
						vFarmlimit.vfarmlimit.checkTimer();
			            
			        	if (profit > 0) {
			        		if (!vFarmlimit.vfarmlimit.farmlimit.containsKey(player.getName()))
			        			vFarmlimit.vfarmlimit.farmlimit.put(player.getName(), 0.0);
				            double farm = vFarmlimit.vfarmlimit.farmlimit.get(player.getName());
			        		farm = farm + profit;        			
		        			
			        		if(farm > vFarmlimit.vfarmlimit.limit(player)) {
			        			player.sendMessage("You reached your farmlimit of " + vFarmlimit.vfarmlimit.limit(player) + " for today. Try again tomorrow!");
			        			event.setCancelled(true);
			        		} else {
			        			vFarmlimit.vfarmlimit.farmlimit.put(player.getName(), farm);
			        		}
			        	}
			        }
            	}
            }
    }
    
}
