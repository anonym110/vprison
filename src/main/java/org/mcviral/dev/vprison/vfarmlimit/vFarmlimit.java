package org.mcviral.dev.vprison.vfarmlimit;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Files;
import org.mcviral.dev.vprison.utilities.Utilities;


public final class vFarmlimit {  
	public static vFarmlimit vfarmlimit;
	@SuppressWarnings("unused")
	private vPrison plugin;
    public HashMap<String, Double> farmlimit = new HashMap<String, Double>();
    public HashMap<String, ItemStack> banned = new HashMap<String, ItemStack>();
	String[] ban = new String[]{"sugar", "pumpkin", "cactus", "string", "bone", "gunpowder", "rottenflesh", "spidereye", "rawpork", "rawchicken", "rawsteak"};
	Files farmyml = new Files("farm.yml");  
	
	public vFarmlimit(vPrison plugin) {
		this.plugin = plugin;
	}
        
    public void initiateBan() {
    	banned.put("sugar", new ItemStack(Material.SUGAR));
    	banned.put("pumpkin", new ItemStack(Material.PUMPKIN));
    	banned.put("cactus", new ItemStack(Material.CACTUS));
    	banned.put("string", new ItemStack(Material.STRING));
    	banned.put("bone", new ItemStack(Material.BONE));
    	banned.put("rottenflesh", new ItemStack(Material.ROTTEN_FLESH));
    	banned.put("spidereye", new ItemStack(Material.SPIDER_EYE));
    	banned.put("rawpork", new ItemStack(Material.PORK));
    	banned.put("rawchicken", new ItemStack(Material.RAW_CHICKEN));
    	banned.put("rawsteak", new ItemStack(Material.RAW_BEEF));
    }
    
    public boolean banned(String word) {
    	for(int i = 0; i < ban.length; i++) {
    		if(word.contains(ban[i]))
    			return true;
    	}
    	return false;
    }
    
    public void saveMap() {
    	File txt = new File("plugins/vPrison/farmlimits.dat");
    	
    	try {
			txt.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

    	try {
			Utilities.save(farmlimit, "plugins/vPrison/farmlimits.dat");
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    @SuppressWarnings("unchecked")
	public void loadMap() {
    	File txt = new File("plugins/vPrison/farmlimits.dat");
    	if (txt.exists()) {
    		try {
    			farmlimit = (HashMap<String, Double>) Utilities.load("plugins/vPrison/farmlimits.dat");
    		} catch (Exception e) {
				e.printStackTrace();
			}
    	}
    }
    

    public ItemStack Material(String desc) {
    	if (banned.containsKey(desc))
    		return banned.get(desc);
    	return null;
    }
    
    public int limit(Player player) {
    	int limit = 500;

    	if(vPrison.perms.has(player, "vPrison.farmlimit.1"))
    		limit = 1000;
    	if(vPrison.perms.has(player, "vPrison.farmlimit.2"))
    		limit = 1500;
    	if(vPrison.perms.has(player, "vPrison.farmlimit.3"))
    		limit = 2000;	
    	if(vPrison.perms.has(player, "vPrison.farmlimit.4"))
    		limit = 25000;
    	
    	if(vPrison.perms.has(player, "vPrison.farmlimit.donor"))
    		limit = 2 * limit;
    	
    	return limit;
    }
	    
    public boolean daypassed() {  	
    	String oldTime = farmyml.getConfig().getString("vfarmlimit.time", "");
    	
		Date time = new Date((new Date().getTime()));
    	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
    	String newTime = fmt.format(time);

    	return !oldTime.equals(newTime);
    }
        
	public void checkTimer() {
		if (daypassed()) {
			farmlimit.clear();
			saveTime();
		}
	}
	
    public void saveTime() {
		Date time = new Date((new Date().getTime()));
    	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
    	farmyml.getConfig().set("vfarmlimit.time", fmt.format(time));
    	farmyml.saveConfig();
    }
}
