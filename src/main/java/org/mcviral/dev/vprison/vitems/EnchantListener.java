package org.mcviral.dev.vprison.vitems;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.mcviral.dev.vprison.vPrison;

public class EnchantListener {
	public static EnchantListener enchantlistener;
	private vPrison plugin;
    public HashMap<Player, Inventory> cstTable = new HashMap<Player, Inventory>();
	
	public EnchantListener(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void onEnchantOpen(InventoryOpenEvent event) {
		if (event.getInventory().getType() == InventoryType.ENCHANTING) {
			event.setCancelled(true);
			
			ItemStack placeholder = new ItemStack(Material.STAINED_GLASS_PANE,1);
			placeholder.setDurability((short) 15);
			ItemMeta pMeta = placeholder.getItemMeta();
			pMeta.setDisplayName(ChatColor.BLACK + "x");
			placeholder.setItemMeta(pMeta);
			
			Player player = (Player) event.getPlayer();
			cstTable.put(player, plugin.getServer().createInventory(player, InventoryType.CHEST, "ENCHANTING"));
			Inventory customInv = cstTable.get(player);
			
			for(int i=0; i < 27; i++) {
				customInv.setItem(i, placeholder);
			}
			
			event.getPlayer().openInventory(customInv);
		}
	}
			

	public void onEnchantTableClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory customInv = cstTable.get(player);
		if(event.getInventory().equals(customInv) && event.getCurrentItem() != null) {				
				event.setCancelled(true);	
		}
	}	


	public void onEnchantTableClose(InventoryCloseEvent event) {
	}

}
