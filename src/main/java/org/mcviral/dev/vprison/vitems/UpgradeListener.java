package org.mcviral.dev.vprison.vitems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.mcviral.dev.vprison.vPrison;

public class UpgradeListener {
	public static UpgradeListener upgradelistener;
	private vPrison plugin;
    public HashMap<Player, Inventory> cstAnvil = new HashMap<Player, Inventory>();
	
	public UpgradeListener(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void onAnvilOpen(InventoryOpenEvent event) {	
		if (event.getInventory().getType() == InventoryType.ANVIL) {
			event.setCancelled(true);

			List<String> lore = new ArrayList<String>();
			ItemStack accept = new ItemStack(Material.WOOL, 1);
			accept.setDurability((short) 5);
			ItemMeta accMeta = accept.getItemMeta();
			accMeta.setDisplayName("Accept");
			lore.add("WARNING");
			lore.add("Going over 60% might break your item.");
			lore.add("Use at own risk!");
			accMeta.setLore(lore);
			accept.setItemMeta(accMeta);
			
			ItemStack placeholder = new ItemStack(Material.STAINED_GLASS_PANE,1);
			placeholder.setDurability((short) 15);
			ItemMeta pMeta = placeholder.getItemMeta();
			pMeta.setDisplayName(ChatColor.BLACK + "x");
			placeholder.setItemMeta(pMeta);
			
			Player player = (Player) event.getPlayer();
			cstAnvil.put(player, plugin.getServer().createInventory(player, InventoryType.CHEST, "UPGRADING"));
			Inventory customInv = cstAnvil.get(player);
			
			for(int i=0; i < 12; i++) {
				customInv.setItem(i, placeholder);
			}
			for(int i=15; i < 17; i++) {
				customInv.setItem(i, placeholder);
			}
			for(int i=18; i < 27; i++) {
				customInv.setItem(i, placeholder);
			}
			customInv.setItem(13, placeholder);
			customInv.setItem(17, accept);
			
			event.getPlayer().openInventory(customInv);
		}		
	}
			
	public void onAnvilClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory customInv = cstAnvil.get(player);
		if(event.getInventory().equals(customInv) && event.getCurrentItem() != null) {	
			ItemStack item = null;
			ItemStack stone = null;
			if(event.getRawSlot() == 17) {
				if(event.getInventory().getItem(12) != null & event.getInventory().getItem(14) != null) {
					if(vItems.vitems.isUpgradeable(event.getInventory().getItem(12)) && Upgrade.upgrading.isStone(event.getInventory().getItem(14))) {
						item = event.getInventory().getItem(12);
						stone = event.getInventory().getItem(14);
						int value = Upgrade.upgrading.StoneValue(stone);
						Upgrade.upgrading.upgrade(item, value, player, customInv);
						if(customInv.getItem(12) != null)
							player.getInventory().addItem(customInv.getItem(12));
						if(stone.getAmount() > 1) {
							stone.setAmount(stone.getAmount() - 1);
							player.getInventory().addItem(stone);
						}
						customInv.clear();
						player.closeInventory();
					} else if(vItems.vitems.isUpgradeable(event.getInventory().getItem(14)) && Upgrade.upgrading.isStone(event.getInventory().getItem(12))) {
						item = event.getInventory().getItem(14);
						stone = event.getInventory().getItem(12);
						int value = Upgrade.upgrading.StoneValue(stone);
						Upgrade.upgrading.upgrade(item, value, player, customInv);						
						if(customInv.getItem(14) != null)
							player.getInventory().addItem(customInv.getItem(14));
						if(stone.getAmount() > 1) {
							stone.setAmount(stone.getAmount() - 1);
							player.getInventory().addItem(stone);
						}
						customInv.clear();
						player.closeInventory();
					} else {
						player.sendMessage("You need a Weapon and a Stone..");	//wrong items
					} 
				} else {
					player.sendMessage("Please add a Weapon and a Stone..");
				}
			}
			
			if(event.getRawSlot() <= 11 || event.getRawSlot() == 13 || (event.getRawSlot() >= 15 && event.getRawSlot() <= 26)) {
				event.setCancelled(true);
			}		
		}
	}	

	public void onAnvilClose(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		Inventory customInv = cstAnvil.get(player);
		if(event.getInventory().equals(customInv)) {
			Inventory playerInv = event.getPlayer().getInventory();
			if(customInv.getItem(12) != null)
				playerInv.addItem(customInv.getItem(12));
			if(customInv.getItem(14) != null)
				playerInv.addItem(customInv.getItem(14));
		}
	}

	public void onPlayerFight(EntityDamageByEntityEvent event) {
		if((event.getDamager() instanceof Player)) {			
			Player player = (Player) event.getDamager();
			ItemStack item = player.getItemInHand();
			if(player.getItemInHand() != null && vItems.vitems.isUpgradeable(item)) {					
				double mult = (Upgrade.upgrading.getPercentage(item)); 
				mult = mult / 100;
		        double oldDMG = event.getDamage();
		        double addDMG = (oldDMG  * mult);
		        addDMG = addDMG / 2;
		        double newDMG = oldDMG + addDMG;       
		        event.setDamage(newDMG);	

		        double Armor = event.getDamage(DamageModifier.ARMOR);
		        double armorpen = mult / 3 + 1;
		        double newArmor = Armor / armorpen;		        
		        event.setDamage(DamageModifier.ARMOR, newArmor);
		        
//	        	plugin.getLogger().info(" ======================== ");  
//	        	plugin.getLogger().info("[OFFENSE] Armor: " + Armor);
//	        	plugin.getLogger().info("[OFFENSE] armorpen: " + armorpen);
//	        	plugin.getLogger().info("[OFFENSE] newArmor: " + newArmor);
//	        	plugin.getLogger().info("[OFFENSE] oldDMG: " + oldDMG);
//	        	plugin.getLogger().info("[OFFENSE] addDMG: " + addDMG);
//	        	plugin.getLogger().info("[OFFENSE] newDMG: " + newDMG);
//	        	plugin.getLogger().info("[OFFENSE] -Armor: " + event.getDamage(DamageModifier.ARMOR));
//	        	plugin.getLogger().info("[OFFENSE] finalDMG: " + event.getFinalDamage());	
		        
//	        	plugin.getLogger().info("[OFFENSE] Armor: " + event.getOriginalDamage(DamageModifier.ARMOR));
//	        	plugin.getLogger().info("[OFFENSE] Armor: " + event.getDamage(DamageModifier.ARMOR));
//	        	plugin.getLogger().info("[OFFENSE] Base: " + event.getOriginalDamage(DamageModifier.BASE));
//	        	plugin.getLogger().info("[OFFENSE] Base: " + event.getDamage(DamageModifier.BASE));		        
//	        	plugin.getLogger().info("[OFFENSE] Absorption: " + event.getOriginalDamage(DamageModifier.ABSORPTION));
//	        	plugin.getLogger().info("[OFFENSE] Absorption: " + event.getDamage(DamageModifier.ABSORPTION));
//	        	plugin.getLogger().info("[OFFENSE] Blocking: " + event.getOriginalDamage(DamageModifier.BLOCKING));
//	        	plugin.getLogger().info("[OFFENSE] Blocking: " + event.getDamage(DamageModifier.BLOCKING));
//	        	plugin.getLogger().info("[OFFENSE] Restisting: " + event.getOriginalDamage(DamageModifier.RESISTANCE));
//	        	plugin.getLogger().info("[OFFENSE] Restisting: " + event.getDamage(DamageModifier.RESISTANCE));
//	        	plugin.getLogger().info("[OFFENSE] Magic: " + event.getDamage(DamageModifier.MAGIC));
//	        	plugin.getLogger().info("[OFFENSE] Magic: " + event.getOriginalDamage(DamageModifier.MAGIC));
			}
		}
		
		if((event.getEntity() instanceof Player)) {			
        	Player player = (Player) event.getEntity();
        	double mult, mult1 = 0, mult2 = 0, mult3 = 0, mult4 = 0;
        	double DMG, finDMG;
        	ItemStack helmet = player.getInventory().getHelmet();        	
        	ItemStack chestplate = player.getInventory().getChestplate();
        	ItemStack leggings = player.getInventory().getLeggings();     	
        	ItemStack boots = player.getInventory().getBoots();
        	
        	if(player.getInventory().getHelmet() != null && vItems.vitems.isUpgradeable(helmet)) {
				mult1 = (Upgrade.upgrading.getPercentage(helmet)); 
				mult1 = mult1 / 100;
        	}
        	if(player.getInventory().getChestplate() != null && vItems.vitems.isUpgradeable(chestplate)) {
				mult2 = (Upgrade.upgrading.getPercentage(chestplate)); 
				mult2 = mult2 / 100;
        	}
        	if(player.getInventory().getLeggings() != null && vItems.vitems.isUpgradeable(leggings)) {
				mult3 = (Upgrade.upgrading.getPercentage(leggings)); 
				mult3 = mult3 / 100;
        	}
        	if(player.getInventory().getBoots() != null && vItems.vitems.isUpgradeable(boots)) {
				mult4 = (Upgrade.upgrading.getPercentage(boots)); 
				mult4 = mult4 / 100;
        	}
        	mult = (mult1 + mult2 + mult3 + mult4) / 10;
        	finDMG = event.getFinalDamage();
        	DMG = finDMG - (finDMG * mult);
    		event.setDamage(DamageModifier.BASE, DMG);
    		event.setDamage(DamageModifier.ARMOR, 0);
    		event.setDamage(DamageModifier.MAGIC, 0);
        	
//        	plugin.getLogger().info("[DEFENSE] mult: " + mult);
//        	plugin.getLogger().info("[DEFENSE] RAW: " + event.getDamage());
//        	plugin.getLogger().info("[DEFENSE] DMG: " + DMG);
//        	plugin.getLogger().info("[DEFENSE] finDMG: " + finDMG);
//        	plugin.getLogger().info("[DEFENSE] FINAL: " + event.getFinalDamage()); 	
//        	plugin.getLogger().info("[DEFENSE] RAW: " + event.getDamage());
//        	plugin.getLogger().info("[DEFENSE] mult: " + mult);
//        	plugin.getLogger().info("[DEFENSE] ARMOR: " + ARMOR);
//        	plugin.getLogger().info("[DEFENSE] newARMOR: " + newARMOR);
//        	plugin.getLogger().info("[DEFENSE] MAGIC: " + MAGIC);
//        	plugin.getLogger().info("[DEFENSE] newMAGIC: " + newMAGIC);
//        	plugin.getLogger().info("[DEFENSE] finalDMG: " + event.getFinalDamage());
//        	plugin.getLogger().info(" ======================== ");  
        	        	

		}
	}
}
