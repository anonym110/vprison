package org.mcviral.dev.vprison.vitems;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Utilities;

public class Upgrade {
	public static Upgrade upgrading;
	@SuppressWarnings("unused")
	private vPrison plugin;
	ItemStack a;
	ItemStack b;
	ItemStack c;
	ItemStack d;
	ItemStack e;
	ItemStack f;
	 
	public Upgrade(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public int getPercentage(ItemStack item) {
		if (!item.getItemMeta().hasDisplayName())
			return 0;
		String name = item.getItemMeta().getDisplayName();
		if(name.charAt(0) == '[') {
			if(name.charAt(3) == ']')
				name = name.substring(1, 2);
			else if(name.charAt(4) == ']')
				name = name.substring(1, 3);
			else if(name.charAt(5) == ']')
				name = name.substring(1, 4);
		} else if(name.charAt(2) == '[') {
			if(name.charAt(5) == ']')
				name = name.substring(3, 4);
			else if(name.charAt(6) == ']')
				name = name.substring(3, 5);
			else if(name.charAt(7) == ']')
				name = name.substring(3, 6);
		} else
			name = "0";
		return Integer.parseInt(name);
	}
		
	public void setPercentage(ItemStack item, int amount, Player player) {
		if (!item.getItemMeta().hasDisplayName())
			return;
		int perc = getPercentage(item);
		String start = "";
		String name = item.getItemMeta().getDisplayName();
		if(name.charAt(0) == '[') {
			if(name.charAt(3) == ']')
				name = name.substring(4);
			else if(name.charAt(4) == ']')
				name = name.substring(5);
			else if(name.charAt(5) == ']')
				name = name.substring(6);
		} else if(name.charAt(2) == '[') {
				start = name.substring(0, 2);
			if(name.charAt(5) == ']')
				name = name.substring(6);
			else if(name.charAt(6) == ']')
				name = name.substring(7);
			else if(name.charAt(7) == ']')
				name = name.substring(8);
		} else {
			start = name.substring(0, 2);
			perc = perc + amount;
			if(perc > 120)
				perc = 120;
			if(perc < 0)
				perc = 0;	
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(start + "[" + perc + "%] " + name);
			item.setItemMeta(im);
			return;
		}
		
			perc = perc + amount;
			if(perc > 120)
				perc = 120;
			if(perc < 0)
				perc = 0;	
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(start + "[" + perc + "%]" + name);
			item.setItemMeta(im);
	}
	
	public void setPercentage(ItemStack item, int amount) {
		if (!item.getItemMeta().hasDisplayName())
			return;
		int perc = getPercentage(item);
		String start = "";
		String name = item.getItemMeta().getDisplayName();
		if(name.charAt(0) == '[') {
			if(name.charAt(3) == ']')
				name = name.substring(4);
			else if(name.charAt(4) == ']')
				name = name.substring(5);
			else if(name.charAt(5) == ']')
				name = name.substring(6);
		} else if(name.charAt(2) == '[') {
				start = name.substring(0, 2);
			if(name.charAt(5) == ']')
				name = name.substring(6);
			else if(name.charAt(6) == ']')
				name = name.substring(7);
			else if(name.charAt(7) == ']')
				name = name.substring(8);
		} else {
			start = name.substring(0, 2);
			perc = perc + amount;
			if(perc > 120)
				perc = 120;
			if(perc < 0)
				perc = 0;	
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(start + "[" + perc + "%] " + name);
			item.setItemMeta(im);
			return;
		}
			perc = perc + amount;
			if(perc > 120)
				perc = 120;
			if(perc < 0)
				perc = 0;	
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(start + "[" + perc + "%]" + name);
			item.setItemMeta(im);
	}
	
	public boolean upgrade(ItemStack item, int amount, Player player) {
		int perc = getPercentage(item);
		int newperc = perc + amount; //THIS WAY OR OTHER WAY??
		
		if(perc < 60) {
			if(success(perc, newperc, getluck(player))) {
				setPercentage(item, amount, player);
				player.sendMessage("success");
				return true;
			} else {
				player.sendMessage("failed");
				setPercentage(item, -3, player);
				return true;
			}			
		} else if(perc >= 60 && perc <= 120) {
			if(success(perc, newperc, getluck(player))) {
				setPercentage(item, amount, player);
			} else {
				if(itembreak()) {
					player.getInventory().remove(item);
					player.sendMessage("Your item broke.");
					return true;
				} else {
					setPercentage(item, -3, player);
					player.sendMessage("failed");
					return true;
				}	
			}	
		}
		return false;
	}

	public void upgrade(ItemStack item, int amount, Player player, Inventory inv) {
		int perc = getPercentage(item);
		int newperc = perc + amount; //THIS WAY OR OTHER WAY??
		
		if(newperc < 60) {
			if(success(perc, newperc, getluck(player))) {
				setPercentage(item, amount, player);
				player.sendMessage("success");
			} else {
				player.sendMessage("failed");
				setPercentage(item, -3, player);
			}			
		} else if(newperc >= 60) {
			if(success(perc, newperc, getluck(player))) {
				setPercentage(item, amount, player);
				player.sendMessage("success");
			} else {
				if(itembreak()) {
					inv.remove(item);
					player.sendMessage("Your item broke.");
				} else {
					setPercentage(item, -3, player);
					player.sendMessage("failed");
				}	
			}	
		}
	}
	
	public boolean success(int perc, int newperc, int luck) {
		double rnd = Utilities.randomWithRange(1, 100);
		if(newperc > 120)
			newperc = 120;
		
		double raw = (200 - perc - newperc)/2;
		double inverse = 100 - raw;
		double gluck = luck * (inverse/100);
		double chance = raw + gluck;
		
//		plugin.getLogger().info(perc + " -> " + newperc);
//		plugin.getLogger().info("raw: " + raw + " inverse: " + inverse + " gluck: " + gluck + " chance: " + chance);
//		
//		plugin.getLogger().info("RND: " + rnd);
		
		if (rnd <= chance)
			return true;
		else
			return false;
	}
	
	public boolean itembreak() {
		int rnd = Utilities.randomWithRange(1, 100);
		if (rnd <= 30)
			return true;
		else
			return false;
	}
	
	public int getluck(Player player) {
		return 30;
	}
	
	public ItemStack itemformat(List<String> lore, String name, ItemStack item) {
//		List<String> lore = new ArrayList<String>();
//		name = ChatColor.BLUE + "[9%] GodSlayer";
//		item = new ItemStack(Material.DIAMOND_SWORD, 1);
		
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		meta.setLore(lore);
		item.setItemMeta(meta);
		
		return item;
	}
	
	public void items() {
		List<String> lore = new ArrayList<String>();
		
		lore.add(ChatColor.RED + "Use to enchant your weapons or armor");
		a = itemformat(lore, ChatColor.RED + "3% Enchant", new ItemStack(Material.COAL, 1));
		b = itemformat(lore, ChatColor.RED + "6% Enchant", new ItemStack(Material.IRON_INGOT, 1));
		c = itemformat(lore, ChatColor.RED + "9% Enchant", new ItemStack(Material.GOLD_INGOT, 1));
		d = itemformat(lore, ChatColor.RED + "12% Enchant", new ItemStack(Material.DIAMOND, 1));
		e = itemformat(lore, ChatColor.RED + "15% Enchant", new ItemStack(Material.EMERALD, 1));
		f = itemformat(lore, ChatColor.RED + "20% Enchant", new ItemStack(Material.NETHER_STAR, 1));

	}
	
	public void test(Player player) {
		player.getInventory().addItem(a);
		player.getInventory().addItem(b);
		player.getInventory().addItem(c);
		player.getInventory().addItem(d);
		player.getInventory().addItem(e);
		player.getInventory().addItem(f);
	}
	
	public boolean isStone(ItemStack item) {
		if(item.isSimilar(a) || item.isSimilar(b) || item.isSimilar(c) || item.isSimilar(d) || item.isSimilar(e) || item.isSimilar(f))
			return true;
		return false;
	}
	
	public int StoneValue(ItemStack item) {
		if(isStone(item)) {
			String name = item.getItemMeta().getDisplayName();
			int value = 0;
			if(name.charAt(3) == '%') {
				value = Integer.parseInt(name.substring(2, 3));
			} else if(name.charAt(4) == '%') {
				value = Integer.parseInt(name.substring(2, 4));
			}
			return value;
		}
		return 0;	
	}
	
}
