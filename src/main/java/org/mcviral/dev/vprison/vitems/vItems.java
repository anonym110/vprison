package org.mcviral.dev.vprison.vitems;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.mcviral.dev.vprison.vPrison;

public class vItems {
	public static vItems vitems;
	@SuppressWarnings("unused")
	private vPrison plugin;
	
	public vItems(vPrison plugin) {
		this.plugin = plugin;
	}
	
	
	public ItemStack Coding() {
		List<String> lore = new ArrayList<String>();
		String name = ChatColor.BLUE + "GodSlayer";
		ItemStack item = new ItemStack(Material.DIAMOND_SWORD, 1);
		
		lore.add(ChatColor.GRAY + "Upgradeable");

		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		meta.setLore(lore);
		item.setItemMeta(meta);
		item.setDurability((short) 10);
		
		return item;
	}
	
	public List<ItemStack> initializeItems() {
		List<ItemStack> finalitems = new ArrayList<ItemStack>();
		List<String> name = new ArrayList<String>();
		List<ItemStack> item = new ArrayList<ItemStack>();
		List<String> lore = new ArrayList<String>();
			
		name.add(ChatColor.RED + "D-Sword");
		name.add(ChatColor.RED + "D-Helmet");
		name.add(ChatColor.RED + "D-Chestplate");
		name.add(ChatColor.RED + "D-Leggings");
		name.add(ChatColor.RED + "D-Boots");
		
		item.add(new ItemStack(Material.DIAMOND_SWORD, 1));
		item.add(new ItemStack(Material.DIAMOND_HELMET, 1));
		item.add(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
		item.add(new ItemStack(Material.DIAMOND_LEGGINGS, 1));
		item.add(new ItemStack(Material.DIAMOND_BOOTS, 1));
		
		lore.add(ChatColor.GRAY + "Upgradeable");
		
		for(int i = 0; i < name.size(); i++) {
		
			String desc = name.get(i);
			ItemStack is = item.get(i);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(desc);
			im.setLore(lore);
			is.setItemMeta(im);
			finalitems.add(is);
		}
		return finalitems;
	}
	
	public void test(Player player) {
		List<ItemStack> items = initializeItems();
		for(int i = 0; i < items.size(); i++) {
			player.getInventory().addItem(items.get(i));
		}
	}
	
	public boolean isUpgradeable(ItemStack item) {
//		if(item == null)
//			return false;
		if(!item.hasItemMeta())
			return false;
		if(item.getItemMeta().hasLore())
			if(item.getItemMeta().getLore().toString().contains("Upgradeable"))
				return true;
		return false;
	}

}
