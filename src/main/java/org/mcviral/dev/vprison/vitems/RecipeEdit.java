package org.mcviral.dev.vprison.vitems;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.mcviral.dev.vprison.vPrison;

public class RecipeEdit {
	public static RecipeEdit recipeedit;
	private vPrison plugin;
	
	public RecipeEdit(vPrison plugin) {
		this.plugin = plugin;
	}

	public void initiateRecipes() {
		IronSword();
		IronHelmet();
		IronChestplate();
		IronLeggings();
		IronBoots();
		GoldSword();
		GoldHelmet();
		GoldChestplate();
		GoldLeggings();
		GoldBoots();
		removeDiamond();
		banItems();
	}
	
	public void clearRecipes(Material material) {
		Iterator<Recipe> it = plugin.getServer().recipeIterator();
		Recipe recipe;
		while(it.hasNext()) {
			recipe = it.next();
			if (recipe != null && recipe.getResult().getType() == material) {
				it.remove(); 
			}
		}
	}
	
	public void banItems() {
		clearRecipes(Material.ANVIL);
		clearRecipes(Material.ENDER_CHEST);
	}
	
	public void removeDiamond() {
		clearRecipes(Material.DIAMOND_SWORD);
		clearRecipes(Material.DIAMOND_HELMET);
		clearRecipes(Material.DIAMOND_CHESTPLATE);
		clearRecipes(Material.DIAMOND_LEGGINGS);
		clearRecipes(Material.DIAMOND_BOOTS);
	}
	
	public void IronSword() {	
		Material mat = Material.IRON_SWORD;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Iron Sword");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("A", "A", "B");
		recipe.setIngredient('A', Material.IRON_BLOCK);
		recipe.setIngredient('B', Material.STICK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void IronHelmet() {	
		Material mat = Material.IRON_HELMET;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Iron Helmet");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("AAA", "A A");
		recipe.setIngredient('A', Material.IRON_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void IronChestplate() {	
		Material mat = Material.IRON_CHESTPLATE;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Iron Chestplate");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("A A", "AAA", "AAA");
		recipe.setIngredient('A', Material.IRON_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void IronLeggings() {	
		Material mat = Material.IRON_LEGGINGS;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Iron Leggings");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("AAA", "A A", "A A");
		recipe.setIngredient('A', Material.IRON_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void IronBoots() {	
		Material mat = Material.IRON_BOOTS;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Iron Boots");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("A A", "A A");
		recipe.setIngredient('A', Material.IRON_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void GoldSword() {	
		Material mat = Material.GOLD_SWORD;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Gold Sword");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("A", "A", "B");
		recipe.setIngredient('A', Material.GOLD_BLOCK);
		recipe.setIngredient('B', Material.STICK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void GoldHelmet() {	
		Material mat = Material.GOLD_HELMET;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Gold Helmet");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("AAA", "A A");
		recipe.setIngredient('A', Material.GOLD_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void GoldChestplate() {	
		Material mat = Material.GOLD_CHESTPLATE;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Gold Chestplate");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("A A", "AAA", "AAA");
		recipe.setIngredient('A', Material.GOLD_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void GoldLeggings() {	
		Material mat = Material.GOLD_LEGGINGS;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Gold Leggings");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("AAA", "A A", "A A");
		recipe.setIngredient('A', Material.GOLD_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
	public void GoldBoots() {	
		Material mat = Material.GOLD_BOOTS;
		clearRecipes(mat);
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta(); 
		im.setDisplayName(ChatColor.AQUA + "Gold Boots");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Upgradeable");
		im.setLore(lore);
		item.setItemMeta(im);
		ShapedRecipe recipe = new ShapedRecipe(new ItemStack(item));
		recipe.shape("A A", "A A");
		recipe.setIngredient('A', Material.GOLD_BLOCK);
		plugin.getServer().addRecipe(recipe);
	}
	
}
