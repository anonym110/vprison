package org.mcviral.dev.vprison.vitems;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.time.Milestones;

public class ItemsCmd  {
	public static ItemsCmd itemscmd;
	@SuppressWarnings("unused")
	private vPrison plugin;
	  
	public ItemsCmd(vPrison plugin) {
		this.plugin = plugin;
	}
		
	
	public void vitem(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
//			Upgrade.upgrading.test(player);
			Milestones.milestones.getOnlineTime(player);
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
		}
	}
	
	public void percent(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
			vItems.vitems.test(player);
				if(vItems.vitems.isUpgradeable(player.getItemInHand()))
					player.sendMessage("Item is upgraded to " + Upgrade.upgrading.getPercentage(player.getItemInHand()) + "%");	
				else
					player.sendMessage("This item is not upgradeable");
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
		}
	}
	
	public void upgrade(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
			Upgrade.upgrading.setPercentage(player.getItemInHand(), Integer.parseInt(args[0]));
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
		}
	}
}