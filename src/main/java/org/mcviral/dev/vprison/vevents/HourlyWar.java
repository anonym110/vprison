package org.mcviral.dev.vprison.vevents;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;

public class HourlyWar {
	public static HourlyWar hourlywar;
	private vPrison plugin;	
	public HashMap<Player, Location> oldLoc = new HashMap<Player, Location>();
	public HashMap<Player, ItemStack[]> oldInv = new HashMap<Player, ItemStack[]>();
	public HashMap<Player, ItemStack[]> oldArmor = new HashMap<Player, ItemStack[]>();
	public HashMap<Player, Double> oldHealth = new HashMap<Player, Double>();
	public HashMap<Player, Integer> oldFood = new HashMap<Player, Integer>();
	public HashMap<Player, GameMode> oldGamemode = new HashMap<Player, GameMode>();
	public ArrayList<Player> players = new ArrayList<Player>();
	public ArrayList<Player> redTeam = new ArrayList<Player>();
	public ArrayList<Player> blueTeam = new ArrayList<Player>();
	public boolean WarActive = false;
	public boolean WarOpen = false;
	public int redScore = 0;
	public int blueScore = 0;
	public int TaskID;
	public String PluginTag = ChatColor.translateAlternateColorCodes('&', "&f[&cWar&f] &7");
	
	public HourlyWar (vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void Schedule() {
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
	  		  public void run() {
	  			  Open();
	  		  }
	  	}, 120*60*20L, 120*60*20L);
	}
	
	public void Join(Player player) {
		if (WarOpen && !players.contains(player)) {	
			oldGamemode.put(player, player.getGameMode());
			oldLoc.put(player, player.getLocation());
			oldInv.put(player, player.getInventory().getContents());
			oldArmor.put(player, player.getInventory().getArmorContents());
			oldHealth.put(player, player.getHealth());
			oldFood.put(player, player.getFoodLevel());
			players.add(player);
			Team(player);
			Teleport(player);
			SetInventory(player);
			player.setGameMode(GameMode.SURVIVAL);
			player.setFoodLevel(20);
			player.setHealth(20);
		} else if(players.contains(player)) {
			player.sendMessage(PluginTag + "Already joined");
		} else {
			player.sendMessage(PluginTag + "War not open");
		}
	}
	
	public void Open() {
		WarOpen = true;
		plugin.getServer().broadcastMessage(PluginTag + "The gates to war have been opened. Type /war join to join the battle!");
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				plugin.getServer().broadcastMessage(PluginTag + "The gates to war have been closed.");
				if(redTeam.size() >= 1 && blueTeam.size() >= 1) {
					Start();
				} else {
					broadcast(PluginTag + "Not enough Players");
					Cancel();
				}
			}
		}, 30*20L); 
	}
	
	public void Start() {
		broadcast(PluginTag + "War started.");
		WarOpen = false;
		WarActive = true;
		GatesOpen();
		TaskID = plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				End();	
			}
		}, 5*60*20L); 
	}
	
	public void End() {
		broadcast(PluginTag + "War ended.");
		WarActive = false;
		for (Player player : players) {
			plugin.getLogger().info(player.getName());
	    	player.getInventory().setContents(oldInv.get(player));
	    	player.getInventory().setArmorContents(oldArmor.get(player));
	    	player.teleport(oldLoc.get(player));
	    	player.setFoodLevel(oldFood.get(player));
	    	player.setHealth(oldHealth.get(player));
	    	player.setGameMode(oldGamemode.get(player));
	    }
		Win();
		players.clear();
		blueTeam.clear();
		redTeam.clear();
		redScore = 0;
		blueScore = 0;
		Signs();
		GatesClose();
	}
	
	public void Cancel() {
		WarOpen = false;
		WarActive = false;
		for (Player player : players) {
	    	player.getInventory().setContents(oldInv.get(player));
	    	player.getInventory().setArmorContents(oldArmor.get(player));
	    	player.teleport(oldLoc.get(player));
	    	player.setFoodLevel(oldFood.get(player));
	    	player.setHealth(oldHealth.get(player));
	    	player.setGameMode(oldGamemode.get(player));
	    }
		players.clear();
		blueTeam.clear();
		redTeam.clear();
		redScore = 0;
		blueScore = 0;		
		Signs();
		GatesClose();
	}
	
	public void SetInventory(Player player) {
		player.getInventory().clear();
		player.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
		player.getInventory().setArmorContents(null);
		ItemStack helmet = new ItemStack(Material.WOOL);
		if(redTeam.contains(player))
			helmet.setDurability((short) 14);
		else if(blueTeam.contains(player))
			helmet.setDurability((short) 11);
		
		player.getInventory().setHelmet(helmet);
		
	}
	
	public void Team(Player player) {
		if (redTeam.size() < blueTeam.size())
			redTeam.add(player);
		else
			blueTeam.add(player);
	}
	
	public void Win() {
		if (redScore > blueScore) {
			for (Player player : redTeam) {
				Reward(player);
			}
		} else if (blueScore > redScore) {
			for (Player player : blueTeam) {
				Reward(player);
			}
		} else {
			plugin.getLogger().info("Draw");
		}
	}
	
	public void Reward(Player player) {
		player.sendMessage(PluginTag + "You win!");
		player.getInventory().addItem(new ItemStack(Material.DIAMOND));
	}
	
	public void Teleport(Player player) {
		Location redSpawn = new Location(plugin.getServer().getWorld("world"), 241, 72, 79, 0, 0);
		Location blueSpawn = new Location(plugin.getServer().getWorld("world"), 241, 72, 105, 180, 0);
		if (redTeam.contains(player))
			player.teleport(redSpawn);
		else
			player.teleport(blueSpawn);
	}
	
	public void ScoreCheck() {
		if(redScore >= 25 || blueScore >= 25) {
			Bukkit.getScheduler().cancelTask(TaskID);
			End();
		}
	}
	
	public void broadcast(String msg) {
		for (Player player : players) {
			player.sendMessage(msg);
		}
	}
	
	public void CombatEvent(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
			if (players.contains(event.getDamager()) && players.contains(event.getEntity())) {
				Player attacker = (Player) event.getDamager();
				Player target = (Player) event.getEntity();
				if(blueTeam.contains(attacker) && blueTeam.contains(target))
					event.setCancelled(true);
				if(redTeam.contains(attacker) && redTeam.contains(target))
					event.setCancelled(true);		
				if(!WarActive)
					event.setCancelled(true);

				if(target.getHealth() - event.getDamage() <= 0) {
					if(redTeam.contains(attacker))
						redScore++;
					if(blueTeam.contains(attacker))
						blueScore++;
					broadcast(PluginTag + ChatColor.AQUA + "" + blueScore + " " + ChatColor.RED + redScore);
					ScoreCheck();
					event.setCancelled(true);
					Teleport(target);
					target.setHealth(20);
					Signs();
				}
			}
		}
	}

	public void DisconnectEvent(PlayerQuitEvent event) {
		Player player = event.getPlayer();	
		if(players.contains(player)) {
			player.sendMessage(PluginTag + "Disconnected from War");
	    	player.getInventory().setContents(oldInv.get(player));
	    	player.getInventory().setArmorContents(oldArmor.get(player));
	    	player.teleport(oldLoc.get(player));
	    	player.setFoodLevel(oldFood.get(player));
	    	player.setHealth(oldHealth.get(player));
	    	if(redTeam.contains(player))
	    		redTeam.remove(player);
	    	if(blueTeam.contains(player))
	    		blueTeam.remove(player);
	    	players.remove(player);
	    	if(WarActive)
	    		PlayersCheck();	
		}
	}
	
	public void HungerEvent(FoodLevelChangeEvent event) {
		Player player = (Player) event.getEntity();
		if(players.contains(player))
			event.setCancelled(true);
	}
	
	public void Leave(Player player) {
		if(players.contains(player)) {
			player.sendMessage(PluginTag + "Left War.");
	    	player.getInventory().setContents(oldInv.get(player));
	    	player.getInventory().setArmorContents(oldArmor.get(player));
	    	player.teleport(oldLoc.get(player));
	    	player.setFoodLevel(oldFood.get(player));
	    	player.setHealth(oldHealth.get(player));
	    	if(redTeam.contains(player))
	    		redTeam.remove(player);
	    	if(blueTeam.contains(player))
	    		blueTeam.remove(player);
	    	players.remove(player);
	    	if(WarActive)
	    		PlayersCheck();	
		}
	}
	
	public void PlayersCheck() {
		if(redTeam.size() < 1 || blueTeam.size() < 1) {
			broadcast(PluginTag + "Too many players disconnected");
			Cancel();
		}
	}
	
	public void cmd(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
			if(args.length == 0 && vPrison.perms.has(player, "vprison.war")) {
				player.sendMessage(PluginTag + "Use /war join or /war start");
				for(int i = 0; i < players.size(); i++) {
					player.sendMessage("Players: " + players.get(i));
				}
			} else if(args[0].equalsIgnoreCase("join") && vPrison.perms.has(player, "vprison.war.join")) {
				if(WarOpen && !players.contains(player)) {
					Join(player);
					player.sendMessage(PluginTag + "Joined War.");
				} else if(WarOpen && players.contains(player)) {
					player.sendMessage(PluginTag + "Already joined.");
				} else
					player.sendMessage(PluginTag + "War is currently closed.");
			} else if(args[0].equalsIgnoreCase("leave") && vPrison.perms.has(player, "vprison.war.join")) {
				if(players.contains(player)) {
					Leave(player);
				}	
			} else if((args[0].equalsIgnoreCase("start") || args[0].equalsIgnoreCase("open")) && vPrison.perms.has(player, "vprison.war.start")) {
				if(!WarOpen && !WarActive)
					Open();
			} else {
				player.sendMessage(PluginTag + "Cmd: " + args[0]);
				
			}
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
		}
	}

	public void Signs() {
		Location red1 = new Location(plugin.getServer().getWorld("world"), 238, 73, 103);
		Location red2 = new Location(plugin.getServer().getWorld("world"), 243, 73, 80);
		Location blue1 = new Location(plugin.getServer().getWorld("world"), 238, 73, 80);
		Location blue2 = new Location(plugin.getServer().getWorld("world"), 243, 73, 103);
		
		Sign sign1 = (Sign) red1.getBlock().getState();
		sign1.setLine(1, ChatColor.RED + "Red Score:");
		sign1.setLine(2, ChatColor.RED+ "" + redScore);
		sign1.update();
		
		Sign sign2 = (Sign) red2.getBlock().getState();
		sign2.setLine(1, ChatColor.RED + "Red Score:");
		sign2.setLine(2, ChatColor.RED+ "" + redScore);
		sign2.update();
		
		Sign sign3 = (Sign) blue1.getBlock().getState();
		sign3.setLine(1, ChatColor.DARK_BLUE + "Blue Score:");
		sign3.setLine(2, ChatColor.DARK_BLUE+ "" + blueScore);
		sign3.update();
		
		Sign sign4 = (Sign) blue2.getBlock().getState();
		sign4.setLine(1, ChatColor.DARK_BLUE + "Blue Score:");
		sign4.setLine(2, ChatColor.DARK_BLUE+ "" + blueScore);
		sign4.update();
	}
	
	public void GatesOpen() {
		for (int x = 240; x < 241 + 1; x++) {
			for (int y = 72; y < 74 + 1; y++) {
				for (int z = 102; z < 102 + 1; z++) {
					Block b = Bukkit.getServer().getWorld("world").getBlockAt(x, y, z);
					b.setType(Material.AIR);
				}
			}
		}
		for (int x = 240; x < 241 + 1; x++) {
			for (int y = 72; y < 74 + 1; y++) {
				for (int z = 81; z < 81 + 1; z++) {
					Block b = Bukkit.getServer().getWorld("world").getBlockAt(x, y, z);
					b.setType(Material.AIR);
				}
			}
		}
	}
	
	public void GatesClose() {
		for (int x = 240; x < 241 + 1; x++) {
			for (int y = 72; y < 74 + 1; y++) {
				for (int z = 102; z < 102 + 1; z++) {
					Block b = Bukkit.getServer().getWorld("world").getBlockAt(x, y, z);
					b.setType(Material.IRON_FENCE);
				}
			}
		}
		for (int x = 240; x < 241 + 1; x++) {
			for (int y = 72; y < 74 + 1; y++) {
				for (int z = 81; z < 81 + 1; z++) {
					Block b = Bukkit.getServer().getWorld("world").getBlockAt(x, y, z);
					b.setType(Material.IRON_FENCE);
				}
			}
		}
	}
	
	public void DeathEvent(PlayerDeathEvent event) { 
		Player player = event.getEntity();
		if(players.contains(player)) {
			if(redTeam.contains(player))
				redScore++;
			else if(blueTeam.contains(player))
				blueScore++;
			event.setKeepLevel(true);
			event.getDrops().clear();
			event.setDroppedExp(0);
		}
	}
	
	public void RespawnEvent(PlayerRespawnEvent event) {           //teleport is not working!
		Player player = event.getPlayer();
		if(players.contains(player)) {
			Location redSpawn = new Location(plugin.getServer().getWorld("world"), 241, 72, 79, 0, 0);
			Location blueSpawn = new Location(plugin.getServer().getWorld("world"), 241, 72, 105, 180, 0);	
			SetInventory(player);
			if(redTeam.contains(player))
				event.setRespawnLocation(redSpawn);
			else if(blueTeam.contains(player))
				event.setRespawnLocation(blueSpawn);
		}
	}
	
	public void HelmetOffEvent(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if(players.contains(player)) {
	        if(event.getSlotType() == InventoryType.SlotType.ARMOR) {
	            event.setCancelled(true);
	        }
		}
	}
	
}
