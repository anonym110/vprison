package org.mcviral.dev.vprison.vprestige;


import java.util.UUID;

import net.mcviral.dev.plugins.comchat.main.ComChatChatEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Files;
import org.mcviral.dev.vprison.utilities.Utilities;

public class PrestigeListener {
	public static PrestigeListener prestigelistener;
	private vPrison plugin;
	Files rankyml = new Files("rank.yml");
	  
	public PrestigeListener(vPrison plugin) {
		this.plugin = plugin;
	}

    public void onPlayerJoin(PlayerJoinEvent event){
            Player player = event.getPlayer();
            if(!player.hasPlayedBefore()) {
            	rankyml.getConfig().set(player.getUniqueId() + ".prestige", "0");
            	rankyml.getConfig().set(player.getUniqueId() + ".name", player.getName());
            	rankyml.saveConfig();
                plugin.getLogger().info("New Player " + player.getName() + " added to Database!");
            }
    }
    
    public void onChat(ComChatChatEvent event) { 
        UUID uuid = event.getUuid();
		String format = event.getPrefix();      
		String prestige = "";
		rankyml.reloadConfig();
		int rank = rankyml.getConfig().getInt(uuid + ".prestige");
		
		if (rank != 0)
			prestige = " " + Utilities.IntegerToRomanNumeral(rank);
				
		if (prestige == null || prestige.isEmpty()) {
			event.setPrefix(format.replace("{PRESTIGE}", ""));   
		} else {   
			event.setPrefix(format.replace("{PRESTIGE}", prestige));
//			event.setPrefix(format.replace("{PRESTIGE}", ChatColor.translateAlternateColorCodes('&', prestige )));
		}
	}

}
