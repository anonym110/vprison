package org.mcviral.dev.vprison.vprestige;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Files;
import org.mcviral.dev.vprison.utilities.Utilities;


public final class vPrestige {
	public static vPrestige vprestige;
	private vPrison plugin;
    public String PluginTag = ChatColor.translateAlternateColorCodes('&', "&f[&cvRank&f] &7");
	Files rankyml = new Files("rank.yml");  
    
	public vPrestige(vPrison plugin) {
		this.plugin = plugin;
	}

	
    public void rankup(Player player) {
    	if(vPrison.perms.playerInGroup(player, "C")) {
    		if(vPrison.econ.has(player, 10000)) {
    			vPrison.econ.withdrawPlayer(player, 10000);
    			vPrison.perms.playerRemoveGroup(null, player, "C");
    			vPrison.perms.playerAddGroup(null, player, "B");
    			player.sendMessage(PluginTag + " You have been promoted to B");
				plugin.getServer().broadcastMessage(PluginTag + ChatColor.YELLOW + player.getName() + ChatColor.GRAY + " has been promoted to " + ChatColor.DARK_GREEN + "B-Block" + ChatColor.GRAY + "!");
			} else {
				player.sendMessage(PluginTag + " Unsufficient Funds");
				player.sendMessage(PluginTag + " You need $10000 for B");
			}		
    	} else if(vPrison.perms.playerInGroup(player, "B")) {
    		if(vPrison.econ.has(player, 35000)) {
    			vPrison.econ.withdrawPlayer(player, 35000);
    			vPrison.perms.playerRemoveGroup(null, player, "B");
    			vPrison.perms.playerAddGroup(null, player, "A");
				player.sendMessage(PluginTag + " You have been promoted to A");
				plugin.getServer().broadcastMessage(PluginTag + ChatColor.YELLOW + player.getName() + ChatColor.GRAY + " has been promoted to " + ChatColor.AQUA + "A-Block" + ChatColor.GRAY + "!");
    		} else {
    			player.sendMessage(PluginTag + " Unsufficient Funds");
				player.sendMessage(PluginTag + " You need $35000 for A");
    		}
    	} else if(vPrison.perms.playerInGroup(player, "A")) {
    		if(vPrison.econ.has(player, 75000)) {
    			vPrison.econ.withdrawPlayer(player, 75000);
    			vPrison.perms.playerRemoveGroup(null, player, "A");
    			vPrison.perms.playerAddGroup(null, player, "Elite");
				player.sendMessage(PluginTag + " You have been promoted to Elite");
				plugin.getServer().broadcastMessage(PluginTag + ChatColor.YELLOW + player.getName() + ChatColor.GRAY + " has been promoted to " + ChatColor.DARK_PURPLE + "E-Block" + ChatColor.GRAY + "!");
    		} else {
    			player.sendMessage(PluginTag + " Unsufficient Funds");
    			player.sendMessage(PluginTag + " You need $75000 for Elite");
    		}
    	} else if(vPrison.perms.playerInGroup(player, "Elite")) {
    		if(vPrison.econ.has(player, 150000)) {
    			vPrison.econ.withdrawPlayer(player, 150000);
    			vPrison.perms.playerRemoveGroup(null, player, "Elite");
    			vPrison.perms.playerAddGroup(null, player, "Free");
				player.sendMessage(PluginTag + " You have been promoted to Free");
				plugin.getServer().broadcastMessage(PluginTag + ChatColor.YELLOW + player.getName() + ChatColor.GRAY + " has been promoted to " + ChatColor.LIGHT_PURPLE + "Free" + ChatColor.GRAY + "!");
    		} else {
    			player.sendMessage(PluginTag + " Unsufficient Funds");
    			player.sendMessage(PluginTag + " You need $150000 for Free");
    		}
    	} else if(vPrison.perms.playerInGroup(player, "Free")) {
    		prestigeRank(player);
    	} else {
    		player.sendMessage(PluginTag + "You can not rank up through this command.");
    	}
    }

    public void prestigeRank(Player player) {
    	int rank = rankyml.getConfig().getInt(player.getUniqueId() + ".prestige");
    	rank++;
    	String prestige = Utilities.IntegerToRomanNumeral(rank);
    	int mult = rank * rank;
    	int price = 1000000 * mult;
    	
		if(vPrison.econ.has(player, price)) {
			vPrison.econ.withdrawPlayer(player, price);
			rankyml.getConfig().set(player.getUniqueId() + ".prestige", rank);
			rankyml.saveConfig();
			player.sendMessage(PluginTag + " You have been promoted to Prestige " + prestige);
//			plugin.chat.setPlayerSuffix(null, player, " [" + prestige + "]");
	    	vPrison.perms.playerAdd(null, player, "ViralPrestige.Prestige." + rank);
	    	plugin.getServer().broadcastMessage(PluginTag + player.getName() + " has been promoted to Prestige " + prestige);
		} else {
			player.sendMessage(PluginTag + " Unsufficient Funds");
			player.sendMessage(PluginTag + " You need " + price + " for Prestige " + prestige);
		}
    }
    
    public void prestigeReward(Player player) {
    	if (rankyml.getConfig().get("config." + rankyml.getConfig().getInt(player.getUniqueId() + ".prestige")) != null) {
//    		dispatchCommand(player, getConfig().getString(("config.commands.") + player.getUniqueId() + ".prestige");
    	}
    }
}
