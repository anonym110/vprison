package org.mcviral.dev.vprison.vprestige;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcviral.dev.vprison.vPrison;


public class PrestigeCmd {
	public static PrestigeCmd prestigecmd;
	@SuppressWarnings("unused")
	private vPrison plugin;
	  
	public PrestigeCmd(vPrison plugin) {
		this.plugin = plugin;
	}
 
//	@Override
	public void rankup(CommandSender sender, Command cmd, String label, String[] args) {
			if ((sender instanceof Player)) {
				Player player = (Player) sender;
				vPrestige.vprestige.rankup(player);
			} else {
				sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
			}
	}
	
//	@Override
	public void prestige(CommandSender sender, Command cmd, String label, String[] args) {
			if ((sender instanceof Player)) {
				Player player = (Player) sender;
				vPrestige.vprestige.rankup(player);
			} else {
				sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
			}
	}
}