package org.mcviral.dev.vprison.vtreefarm;


import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Files;

public class TreefarmListener {
	public static TreefarmListener treefarmListener;
	@SuppressWarnings("unused")
	private vPrison plugin;
	Files treeyml = new Files("treefarm.yml");
	
	public TreefarmListener(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void treeConfig() {		
//		if(treeyml.getConfig().contains("Config.coords.xMin"))
			treeyml.getConfig().set("coords.xMin", 0);
//		if(treeyml.getConfig().contains("Config.coords.xMax"))
			treeyml.getConfig().set("coords.xMax", 0);
//		if(treeyml.getConfig().contains("Config.coords.yMin"))
			treeyml.getConfig().set("coords.yMin", 0);
//		if(treeyml.getConfig().contains("Config.coords.yMax"))
			treeyml.getConfig().set("coords.yMax", 0);
//		if(treeyml.getConfig().contains("Config.coords.zMin"))
			treeyml.getConfig().set("coords.zMin", 0);
//		if(treeyml.getConfig().contains("Config.coords.zMax"))
			treeyml.getConfig().set("coords.zMax", 0);
		treeyml.saveConfig();
	}
	
	public boolean isTreefarm(int x, int y, int z) {
		if (x >= treeyml.getConfig().getInt("coords.xMin") && x <= treeyml.getConfig().getInt("coords.xMax")
		&& y >= treeyml.getConfig().getInt("coords.yMin") && y <= treeyml.getConfig().getInt("coords.yMax")
		&& z >= treeyml.getConfig().getInt("coords.zMin") && z <= treeyml.getConfig().getInt("coords.zMax")){	
			return true;
	  	}
		return false;
	}
	
	
	public void treefarmGriefing(BlockPlaceEvent event)	{
		if (isTreefarm(event.getBlock().getX(), event.getBlock().getY(), event.getBlock().getZ()))
				if (!event.getPlayer().isOp() && event.getPlayer().getGameMode() != GameMode.CREATIVE && !event.isCancelled() && !event.getPlayer().hasPermission("treefarm.build"))
					event.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	public void choppingTree(BlockBreakEvent event)	{
		if (isTreefarm(event.getBlock().getX(), event.getBlock().getY(), event.getBlock().getZ())) {
			if (event.getBlock().getType() == Material.SAPLING && !event.isCancelled() && event.getPlayer().getGameMode() != GameMode.CREATIVE && !event.getPlayer().hasPermission("treefarm.destroy"))
				event.setCancelled(true);
			Location loc = event.getBlock().getLocation();	
			loc.setY(loc.getY() - 1);
			if (loc.getBlock().getType() == Material.DIRT){
				if (event.getBlock().getType() == Material.LOG && !event.isCancelled() && event.getPlayer().getGameMode() != GameMode.CREATIVE){
	                event.setCancelled(true);
	                ItemStack drop = new ItemStack(Material.LOG);
	                drop.setDurability((short) 2);
	                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
	                event.getBlock().setType((Material.SAPLING));
	                event.getBlock().setData((byte) 2);
				}
			}
		}
	}	
	
	public void onLeafDecay(LeavesDecayEvent event)	{
		if (isTreefarm(event.getBlock().getX(), event.getBlock().getY(), event.getBlock().getZ())) {
			event.setCancelled(true);
			event.getBlock().setType(Material.AIR);
		}
	}
}
