package org.mcviral.dev.vprison;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcviral.dev.vprison.time.LoginReward;
import org.mcviral.dev.vprison.time.Milestones;
import org.mcviral.dev.vprison.utilities.Time;
import org.mcviral.dev.vprison.utilities.Utilities;
import org.mcviral.dev.vprison.vchat.Announce;
import org.mcviral.dev.vprison.vchat.ChatListener;
import org.mcviral.dev.vprison.vcrates.CrateDrops;
import org.mcviral.dev.vprison.vcrates.CratesCmd;
import org.mcviral.dev.vprison.vcrates.CratesListener;
import org.mcviral.dev.vprison.vcrates.vCrates;
import org.mcviral.dev.vprison.vevents.HourlyWar;
import org.mcviral.dev.vprison.vfarmlimit.FarmlimitCmd;
import org.mcviral.dev.vprison.vfarmlimit.FarmlimitListener;
import org.mcviral.dev.vprison.vfarmlimit.vFarmlimit;
import org.mcviral.dev.vprison.vfixes.FishingDrops;
import org.mcviral.dev.vprison.vfixes.MoneyFix;
import org.mcviral.dev.vprison.vfixes.NoHunger;
import org.mcviral.dev.vprison.vfixes.SBDF;
import org.mcviral.dev.vprison.vitems.EnchantListener;
import org.mcviral.dev.vprison.vitems.vItems;
import org.mcviral.dev.vprison.vitems.RecipeEdit;
import org.mcviral.dev.vprison.vitems.Upgrade;
import org.mcviral.dev.vprison.vitems.UpgradeListener;
import org.mcviral.dev.vprison.vitems.ItemsCmd;
import org.mcviral.dev.vprison.vprestige.PrestigeCmd;
import org.mcviral.dev.vprison.vprestige.PrestigeListener;
import org.mcviral.dev.vprison.vprestige.vPrestige;
import org.mcviral.dev.vprison.vpvp.PVPRewards;
import org.mcviral.dev.vprison.vpvp.RewardShop;
import org.mcviral.dev.vprison.vscoreboard.ScoreboardListener;
import org.mcviral.dev.vprison.vscoreboard.vScoreboard;
import org.mcviral.dev.vprison.vtreefarm.TreefarmListener;


public final class vPrison extends JavaPlugin {
    public static Economy econ = null;
    public static Permission perms = null;
    public static Chat chat = null;

	@Override
    public void onEnable() {
		getLogger().info(ChatColor.LIGHT_PURPLE + "[mcViral] " + ChatColor.RESET + "vPrisonSuite has loaded. Enjoy nextGen gaming!");
 
		Utilities.utilities = new Utilities(this);
		vItems.vitems = new vItems(this);
		ItemsCmd.itemscmd = new ItemsCmd(this);
		RecipeEdit.recipeedit = new RecipeEdit(this);
		Upgrade.upgrading = new Upgrade(this);
		UpgradeListener.upgradelistener = new UpgradeListener(this);
		EnchantListener.enchantlistener = new EnchantListener(this);
		vScoreboard.vscoreboard = new vScoreboard(this);
		ScoreboardListener.scoreboardlistener = new ScoreboardListener(this);
		vPrestige.vprestige = new vPrestige(this);
		PrestigeListener.prestigelistener = new PrestigeListener(this);
		PrestigeCmd.prestigecmd = new PrestigeCmd(this);
		vFarmlimit.vfarmlimit = new vFarmlimit(this);
		FarmlimitCmd.farmlimitcmd = new FarmlimitCmd(this);
		FarmlimitListener.farmlimitlistener = new FarmlimitListener(this);
		vCrates.vcrates = new vCrates(this);
		CratesListener.crateslistener = new CratesListener(this);
		CratesCmd.cratescmd = new CratesCmd(this);
		MoneyFix.moneyfix = new MoneyFix(this);
		ChatListener.chatlistener = new ChatListener(this);
		Time.time = new Time(this);
		TreefarmListener.treefarmListener = new TreefarmListener(this);
		SBDF.sbdf = new SBDF(this);
		FishingDrops.fishingdrops = new FishingDrops(this);
		Announce.announce = new Announce(this);
		HourlyWar.hourlywar = new HourlyWar(this);
		PVPRewards.pvprewards = new PVPRewards(this);
		RewardShop.rewardshop = new RewardShop(this);
		LoginReward.loginreward = new LoginReward(this);
		Milestones.milestones = new Milestones(this);
		CrateDrops.cratedrops = new CrateDrops(this);
		NoHunger.nohunger = new NoHunger(this);
		
		Upgrade.upgrading.items();
		RecipeEdit.recipeedit.initiateRecipes();
		vFarmlimit.vfarmlimit.loadMap();
		vFarmlimit.vfarmlimit.initiateBan();
		HourlyWar.hourlywar.Schedule();
//		TreefarmListener.treefarmListener.treeConfig();
		
    	this.getCommand("vitem").setExecutor(new mcCommands(this)); 
    	this.getCommand("percent").setExecutor(new mcCommands(this)); 
    	this.getCommand("upgrade").setExecutor(new mcCommands(this)); 
    	this.getCommand("prestige").setExecutor(new mcCommands(this)); 
    	this.getCommand("rankup").setExecutor(new mcCommands(this)); 
       	this.getCommand("farmlimit").setExecutor(new mcCommands(this)); 
       	this.getCommand("ticket").setExecutor(new mcCommands(this));
       	this.getCommand("vbroad").setExecutor(new mcCommands(this));
       	this.getCommand("war").setExecutor(new mcCommands(this));
       	this.getCommand("addvotekey").setExecutor(new mcCommands(this));
       	this.getCommand("addvotecrate").setExecutor(new mcCommands(this));
       	this.getCommand("pvpreward").setExecutor(new mcCommands(this));
       	this.getCommand("achievement").setExecutor(new mcCommands(this));
       	
    	getServer().getPluginManager().registerEvents(new mcListener(this), this);
    	
        if (!setupEconomy() ) {
        	getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        setupPermissions();
        setupChat();
    }
 
    @Override
    public void onDisable() {
    	Milestones.milestones.shutdown();
    	vFarmlimit.vfarmlimit.saveMap();
    	HourlyWar.hourlywar.Cancel();
    }
    
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }   

    
}
