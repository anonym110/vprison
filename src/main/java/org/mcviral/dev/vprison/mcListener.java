package org.mcviral.dev.vprison;

import net.ess3.api.events.SignInteractEvent;
import net.ess3.api.events.UserBalanceUpdateEvent;
import net.mcviral.dev.plugins.comchat.main.ComChatChatEvent;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.mcviral.dev.vprison.time.LoginReward;
import org.mcviral.dev.vprison.time.Milestones;
import org.mcviral.dev.vprison.vcrates.CrateDrops;
import org.mcviral.dev.vprison.vcrates.CratesListener;
import org.mcviral.dev.vprison.vevents.HourlyWar;
import org.mcviral.dev.vprison.vfarmlimit.FarmlimitListener;
import org.mcviral.dev.vprison.vfixes.FishingDrops;
import org.mcviral.dev.vprison.vfixes.NoHunger;
import org.mcviral.dev.vprison.vfixes.SBDF;
import org.mcviral.dev.vprison.vitems.EnchantListener;
import org.mcviral.dev.vprison.vitems.UpgradeListener;
import org.mcviral.dev.vprison.vprestige.PrestigeListener;
import org.mcviral.dev.vprison.vpvp.PVPRewards;
import org.mcviral.dev.vprison.vpvp.RewardShop;
import org.mcviral.dev.vprison.vscoreboard.ScoreboardListener;
import org.mcviral.dev.vprison.vtreefarm.TreefarmListener;

public class mcListener implements Listener {
	@SuppressWarnings("unused")
	private vPrison plugin;
	 
	public mcListener(vPrison plugin) {
		this.plugin = plugin;
	}
    
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		ScoreboardListener.scoreboardlistener.onPlayerJoin(event);
		PrestigeListener.prestigelistener.onPlayerJoin(event);
//		Time.time.login(event.getPlayer());
		Milestones.milestones.login(event);
		LoginReward.loginreward.LoginEvent(event);
	}
	
	@EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
    	ScoreboardListener.scoreboardlistener.onPlayerQuit(event);
//		Time.time.logout(event.getPlayer());
    	Milestones.milestones.logout(event);
		HourlyWar.hourlywar.DisconnectEvent(event);
    }
	
	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent event) {
		EnchantListener.enchantlistener.onEnchantOpen(event);
		UpgradeListener.upgradelistener.onAnvilOpen(event);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		EnchantListener.enchantlistener.onEnchantTableClick(event);
		UpgradeListener.upgradelistener.onAnvilClick(event);
		CratesListener.crateslistener.onCrateOpen(event);
		CratesListener.crateslistener.onBoxClick(event);
		RewardShop.rewardshop.ShopClick(event);
		LoginReward.loginreward.InterfaceClick(event);
		HourlyWar.hourlywar.HelmetOffEvent(event);
	}	

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		EnchantListener.enchantlistener.onEnchantTableClose(event);
		UpgradeListener.upgradelistener.onAnvilClose(event);
	}
	
	@EventHandler
	public void onPlayerFight(EntityDamageByEntityEvent event) {
		UpgradeListener.upgradelistener.onPlayerFight(event);
		HourlyWar.hourlywar.CombatEvent(event);
	}
	
	@EventHandler
    public void onSignInteract(SignInteractEvent event){
		FarmlimitListener.farmlimitlistener.onSignInteract(event);
	}
	
	@EventHandler
    public void onChat(ComChatChatEvent event) { 
		PrestigeListener.prestigelistener.onChat(event);
	}
	
	@EventHandler
    public void onBalChange(UserBalanceUpdateEvent event){
		ScoreboardListener.scoreboardlistener.onBalChange(event);
    }
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event)	{
		TreefarmListener.treefarmListener.treefarmGriefing(event);
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)	{
		TreefarmListener.treefarmListener.choppingTree(event);
		SBDF.sbdf.onSnowblockBreak(event);
		Milestones.milestones.blockBreak(event);
		CrateDrops.cratedrops.onBlockBreak(event);
	}	
	
	@EventHandler
	public void onLeafDecay(LeavesDecayEvent event)	{
		TreefarmListener.treefarmListener.onLeafDecay(event);
	}
	
	@EventHandler
	public void onPlayerFishing(PlayerFishEvent event)	{
		FishingDrops.fishingdrops.onPlayerFish(event);
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event)	{
		PVPRewards.pvprewards.onPVPKill(event);
		HourlyWar.hourlywar.DeathEvent(event);
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event)	{
		HourlyWar.hourlywar.HungerEvent(event);
		NoHunger.nohunger.HungerEvent(event);
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event)	{
		HourlyWar.hourlywar.RespawnEvent(event);
	}
}
