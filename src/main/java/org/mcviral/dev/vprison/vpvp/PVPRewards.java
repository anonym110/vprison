package org.mcviral.dev.vprison.vpvp;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Files;

public class PVPRewards {
	private vPrison plugin;
	public static PVPRewards pvprewards;
	Files pvpyml = new Files("pvp.yml");  
	String PluginTag = ChatColor.translateAlternateColorCodes('&', "&f[&5PvP Rewards&f]&7 ");
	
	public PVPRewards(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public int getKills(Player player) {
		pvpyml.reloadConfig();
		int current = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".kills"))
			current = pvpyml.getConfig().getInt(player.getUniqueId() + ".kills");
		return current;
	}
	
	public int getDeaths(Player player) {
		pvpyml.reloadConfig();
		int current = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".death"))
			current = pvpyml.getConfig().getInt(player.getUniqueId() + ".death");
		return current;	
	}
	
	public double getKDR(Player player) {
		pvpyml.reloadConfig();
		int kills = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".kills"))
			kills = pvpyml.getConfig().getInt(player.getUniqueId() + ".kills");
		int deaths = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".death"))
			deaths = pvpyml.getConfig().getInt(player.getUniqueId() + ".death");

		double lkills = kills;
		double ldeaths = deaths;
		
		if (deaths != 0)
			return lkills/ldeaths;
		else
			return lkills;	
	}
	
	public int getCP(Player player) {
		pvpyml.reloadConfig();
		int current = pvpyml.getConfig().getInt(player.getUniqueId() + ".cp");
		return current;	
	}
	
	public void addKill(Player player, int amount) {
		int current = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".kills"))
			current = pvpyml.getConfig().getInt(player.getUniqueId() + ".kills");
		int newValue = current + amount;
		pvpyml.getConfig().set(player.getUniqueId() + ".kills", newValue);
		pvpyml.saveConfig();
	}
	
	public void addDeath(Player player, int amount) {
		int current = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".death"))
			current = pvpyml.getConfig().getInt(player.getUniqueId() + ".death");
		int newValue = current + amount;
		pvpyml.getConfig().set(player.getUniqueId() + ".death", newValue);
		pvpyml.saveConfig();
	}
	
	public void addCP(Player player, int amount) {
		int current = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".cp"))
			current = pvpyml.getConfig().getInt(player.getUniqueId() + ".cp");
		int newValue = current + amount;
		pvpyml.getConfig().set(player.getUniqueId() + ".cp", newValue);
		pvpyml.saveConfig();
	}
	
	public void removeCP(Player player, int amount) {
		int current = 0;
		if(pvpyml.getConfig().contains(player.getUniqueId() + ".cp"))
			current = pvpyml.getConfig().getInt(player.getUniqueId() + ".cp");
		int newValue = current - amount;
		pvpyml.getConfig().set(player.getUniqueId() + ".cp", newValue);
		pvpyml.saveConfig();
	}
	
	public void setKills(Player player, int amount) {
		pvpyml.getConfig().set(player.getUniqueId() + ".kills", amount);
		pvpyml.saveConfig();
	}
	
	public void setDeaths(Player player, int amount) {
		pvpyml.getConfig().set(player.getUniqueId() + ".death", amount);
		pvpyml.saveConfig();
	}
	
	public void setCP(Player player, int amount) {
		pvpyml.getConfig().set(player.getUniqueId() + ".cp", amount);
		pvpyml.saveConfig();
	}
		
	public void onPVPKill(PlayerDeathEvent event) {
		if(event.getEntity().getKiller() instanceof Player) {
			Player Killer = event.getEntity().getKiller();
			Player Victim = event.getEntity();
			addKill(Killer, 1);
			addCP(Killer, 1);
			addDeath(Victim, 1);
		}
	}
	
	public void Ranking(Player player) {
		
	}

	@SuppressWarnings("deprecation")
	public void Command(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
			if(args.length == 0) {
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "- &aPvP Rewards &7-"));
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "Use &c/pr points &7- displays current Contribution Points"));
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "Use &c/pr shop &7- opens the PvP Shop"));
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "Use &c/pr scoreboard &7- shows the current scoreboard."));
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "Use &c/pr setCP <points> <name> &7- to set Contribution Points."));
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "Use &c/pr addCP <points> <name> &7- to add Contribution Points."));
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "Use &c/pr kdr &7- to display the kdr."));
				player.sendMessage(PluginTag + ChatColor.translateAlternateColorCodes('&', "Use &c/pr stats &7- to display the stats."));
			} else if((args[0].equalsIgnoreCase("points") || args[0].equalsIgnoreCase("cp")) && args.length == 1) {
				player.sendMessage(PluginTag + "Your Contribution Points: " + getCP(player));
			} else if((args[0].equalsIgnoreCase("points") || args[0].equalsIgnoreCase("cp")) && args.length >= 1 && vPrison.perms.has(player, "vprison.pvp.admin")) {
				player.sendMessage(PluginTag + "Your Contribution Points: " + getCP(plugin.getServer().getPlayer(args[1])));
			} else if(args[0].equalsIgnoreCase("scoreboard") || args[0].equalsIgnoreCase("leaderboard") || args[0].equalsIgnoreCase("top")) {
				Ranking(player);
			} else if(args[0].equalsIgnoreCase("shop") || args[0].equalsIgnoreCase("store")) {
				RewardShop.rewardshop.OpenShop(player);
			} else if(args[0].equalsIgnoreCase("setCP") && vPrison.perms.has(player, "vprison.pvp.admin")) {
				setCP(player, Integer.parseInt(args[1]));
				player.sendMessage(PluginTag + "New CP set to " + ChatColor.WHITE + getCP(player));
			} else if(args[0].equalsIgnoreCase("addCP") && vPrison.perms.has(player, "vprison.pvp.admin")) {
				addCP(player, Integer.parseInt(args[1]));
				player.sendMessage(PluginTag + "New CP set to " + ChatColor.WHITE + getCP(player));
			} else if(args[0].equalsIgnoreCase("kdr")) {
				player.sendMessage(PluginTag + "Your Kill/Death Ratio is: " + getKDR(player));
			} else if(args[0].equalsIgnoreCase("stats") && args.length == 1) {
				player.sendMessage(PluginTag + "Kills: " + getKills(player));
				player.sendMessage(PluginTag + "Deaths: " + getDeaths(player));
				player.sendMessage(PluginTag + "KDR: " + getKDR(player));
			} else if(args[0].equalsIgnoreCase("stats") && args.length >= 1 && vPrison.perms.has(player, "vprison.pvp.admin")) {
				player.sendMessage(PluginTag + "Kills: " + getKills(plugin.getServer().getPlayer(args[1])));
				player.sendMessage(PluginTag + "Deaths: " + getDeaths(plugin.getServer().getPlayer(args[1])));
				player.sendMessage(PluginTag + "KDR: " + getKDR(plugin.getServer().getPlayer(args[1])));
			} else {
				player.sendMessage(PluginTag + "Cmd: " + args);
			}
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
		}
	}

	
}
