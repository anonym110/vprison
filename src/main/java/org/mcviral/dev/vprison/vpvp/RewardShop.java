package org.mcviral.dev.vprison.vpvp;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Custom;

public class RewardShop {
	private vPrison plugin;
	public static RewardShop rewardshop;
	public HashMap<Player, Inventory> cstInv = new HashMap<Player, Inventory>();
	String PluginTag = ChatColor.translateAlternateColorCodes('&', "&f[&5PvP Rewards&f]&7 ");
	
	public RewardShop(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void OpenShop(Player player) {
		
		cstInv.put(player, plugin.getServer().createInventory(player, InventoryType.CHEST, ChatColor.DARK_PURPLE + "PVP Reward Shop (Balance: " + PVPRewards.pvprewards.getCP(player) + ")"));
		Inventory customInv = cstInv.get(player);

		customInv.setItem(0, Custom.Item(ChatColor.RED + "The Bloodsword", new ItemStack(Material.DIAMOND_SWORD), Custom.lore(ChatColor.translateAlternateColorCodes('&', "&7Price: &e500 &cCP"))));
		customInv.setItem(1, Custom.Item(ChatColor.RED + "Bloodkeepers Helmet", new ItemStack(Material.DIAMOND_HELMET), Custom.lore(ChatColor.translateAlternateColorCodes('&', "&7Price: &e250 &cCP"))));
		customInv.setItem(2, Custom.Item(ChatColor.RED + "Bloodkeepers Plate", new ItemStack(Material.DIAMOND_CHESTPLATE), Custom.lore(ChatColor.translateAlternateColorCodes('&', "&7Price: &e400 &cCP"))));
		customInv.setItem(3, Custom.Item(ChatColor.RED + "Bloodkeepers Pants", new ItemStack(Material.DIAMOND_LEGGINGS), Custom.lore(ChatColor.translateAlternateColorCodes('&', "&7Price: &e350 &cCP"))));
		customInv.setItem(4, Custom.Item(ChatColor.RED + "Bloodkeepers Boots", new ItemStack(Material.DIAMOND_BOOTS), Custom.lore(ChatColor.translateAlternateColorCodes('&', "&7Price: &e200 &cCP"))));
		
		player.openInventory(customInv);
		
	}
	
	public void ShopClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory customInv = cstInv.get(player);
		
		if(event.getInventory().equals(customInv)) {
			event.setCancelled(true);
			
			if(event.getSlot() == 0) {
				Shopping(player, Custom.Item(ChatColor.RED + "The Bloodsword", new ItemStack(Material.DIAMOND_SWORD), Custom.lore(ChatColor.GRAY + "Upgradeable")), 500);
			} else if(event.getSlot() == 1) {
				Shopping(player, Custom.Item(ChatColor.RED + "Bloodkeepers Helmet", new ItemStack(Material.DIAMOND_HELMET), Custom.lore(ChatColor.GRAY + "Upgradeable")), 250);
			} else if(event.getSlot() == 2) {
				Shopping(player, Custom.Item(ChatColor.RED + "Bloodkeepers Plate", new ItemStack(Material.DIAMOND_CHESTPLATE), Custom.lore(ChatColor.GRAY + "Upgradeable")), 400);
			} else if(event.getSlot() == 3) {
				Shopping(player, Custom.Item(ChatColor.RED + "Bloodkeepers Pants", new ItemStack(Material.DIAMOND_LEGGINGS), Custom.lore(ChatColor.GRAY + "Upgradeable")), 350);
			} else if(event.getSlot() == 4) {
				Shopping(player, Custom.Item(ChatColor.RED + "Bloodkeepers Boots", new ItemStack(Material.DIAMOND_BOOTS), Custom.lore(ChatColor.GRAY + "Upgradeable")), 200);
			}
		}
	}
	
	public void Shopping(Player player, ItemStack item, int price) {
		if(PVPRewards.pvprewards.getCP(player) >= price) {
			PVPRewards.pvprewards.removeCP(player, price);
			player.getInventory().addItem(item);
			player.closeInventory();
		} else {
			player.sendMessage(PluginTag + "You dont have enough Contribution Points.");
		}
	}
	

}
