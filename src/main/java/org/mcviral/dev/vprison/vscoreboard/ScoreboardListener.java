package org.mcviral.dev.vprison.vscoreboard;

import net.ess3.api.events.UserBalanceUpdateEvent;

import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.mcviral.dev.vprison.vPrison;

public class ScoreboardListener {
	public static ScoreboardListener scoreboardlistener;
	private vPrison plugin;
	
	public ScoreboardListener(vPrison plugin) {
		this.plugin = plugin;
	}

    public void onPlayerJoin(PlayerJoinEvent event){
    	plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
   		 
  		  public void run() {
  			  vScoreboard.vscoreboard.allRefresh();
  		  }
  		}, 20L);
    }
    
    public void onBalChange(UserBalanceUpdateEvent event){
    	vScoreboard.vscoreboard.playerRefresh(event.getPlayer());
    }
    
    public void onPlayerQuit(PlayerQuitEvent event){
    	plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    		 
    		  public void run() {
    			  vScoreboard.vscoreboard.allRefresh();
    		  }
    		}, 20L);
    	
    }

}
