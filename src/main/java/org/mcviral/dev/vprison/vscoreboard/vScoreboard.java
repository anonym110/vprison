package org.mcviral.dev.vprison.vscoreboard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.vfixes.MoneyFix;

import com.earth2me.essentials.Essentials;

public class vScoreboard {
	public static vScoreboard vscoreboard;
	private vPrison plugin;
	
	public vScoreboard(vPrison plugin) {
		this.plugin = plugin;
	}
    
    public Scoreboard scoreboard(Player player) {
        Scoreboard board;
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        board = manager.getNewScoreboard();
        Objective objective = board.registerNewObjective("BehindBars", "BehindBars");
        objective.setDisplayName(ChatColor.GRAY + "�" + ChatColor.GOLD + "�" + ChatColor.GRAY + "�" + ChatColor.YELLOW + " BehindBars " + ChatColor.GRAY + "�" + ChatColor.GOLD + "�" + ChatColor.GRAY + "�");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        int a = visiblePlayers();
        int b = Bukkit.getMaxPlayers();
        Score score1 = objective.getScore(ChatColor.GRAY + "Players:");
        score1.setScore(15);
        Score score2 = objective.getScore(ChatColor.GRAY + "" + a + "/" + b);
        score2.setScore(14);
        Score score3 = objective.getScore(ChatColor.GRAY + "-----------");
        score3.setScore(13);
        Score score4 = objective.getScore(ChatColor.GRAY + "Money");
        score4.setScore(12);
        Score score5 = objective.getScore(ChatColor.GRAY + "$" + MoneyFix.moneyfix.convertBal(player));
        score5.setScore(11);
        Score score6 = objective.getScore(ChatColor.GRAY + " ");
        score6.setScore(10);
        
        return board;
    }
    
    public void allRefresh() {
    	for(Player player : plugin.getServer().getOnlinePlayers()) {
    		player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
    		player.setScoreboard(scoreboard(player));
    	}
    }
    
    public void playerRefresh(Player player) {
    	player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		player.setScoreboard(scoreboard(player));
    }
    
    public int visiblePlayers() {
    	Essentials ess = (Essentials) plugin.getServer().getPluginManager().getPlugin("Essentials");
    	int i = 0;
    	
    	for(Player player : plugin.getServer().getOnlinePlayers()) {
    		if(ess.getUser(player).isHidden())
    			i++;
    	}
    	
		return Bukkit.getOnlinePlayers().size() - i;
    }
    
}
