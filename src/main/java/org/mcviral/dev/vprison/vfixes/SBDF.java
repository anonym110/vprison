package org.mcviral.dev.vprison.vfixes;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;

public class SBDF {
	public static SBDF sbdf;
	@SuppressWarnings("unused")
	private vPrison plugin;
	
	public SBDF(vPrison plugin) {
		this.plugin = plugin;
	}

    public void onSnowblockBreak(BlockBreakEvent event){
    	if(!event.isCancelled() && event.getBlock().getType().equals(Material.SNOW_BLOCK) && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
    		event.getBlock().breakNaturally(new ItemStack(Material.SNOW_BLOCK));
    		event.getBlock().getLocation().getWorld().dropItemNaturally(event.getBlock().getLocation(), new ItemStack(Material.SNOW_BLOCK));
    		if(isTool(event.getPlayer().getItemInHand())) {
    			event.getPlayer().getItemInHand().setDurability((short) (event.getPlayer().getItemInHand().getDurability() + 1));
        		if(shouldBreak(event.getPlayer().getItemInHand()))
        			event.getPlayer().getInventory().removeItem(event.getPlayer().getItemInHand());
    		}
    	}
    } 
    
    public boolean isTool(ItemStack item) {
		if(item.getType().equals(Material.DIAMOND_SPADE))
			return true;
		if(item.getType().equals(Material.IRON_SPADE))
			return true;
		if(item.getType().equals(Material.GOLD_SPADE))
			return true;
		if(item.getType().equals(Material.STONE_SPADE))
			return true;
		if(item.getType().equals(Material.WOOD_SPADE))
			return true;
		if(item.getType().equals(Material.DIAMOND_AXE))
			return true;
		if(item.getType().equals(Material.IRON_AXE))
			return true;
		if(item.getType().equals(Material.GOLD_AXE))
			return true;
		if(item.getType().equals(Material.STONE_AXE))
			return true;
		if(item.getType().equals(Material.WOOD_AXE))
			return true;
		if(item.getType().equals(Material.DIAMOND_PICKAXE))
			return true;
		if(item.getType().equals(Material.IRON_PICKAXE))
			return true;
		if(item.getType().equals(Material.GOLD_PICKAXE))
			return true;
		if(item.getType().equals(Material.STONE_PICKAXE))
			return true;
		if(item.getType().equals(Material.WOOD_PICKAXE))
			return true;
		return false;
    }
    
    public boolean shouldBreak(ItemStack item) {
    	int dura = item.getDurability();
		
    	if(item.getType().equals(Material.DIAMOND_SPADE) && dura >= 1562)
			return true;
		if(item.getType().equals(Material.IRON_SPADE) && dura >= 251)
			return true;
		if(item.getType().equals(Material.GOLD_SPADE) && dura >= 33)
			return true;
		if(item.getType().equals(Material.STONE_SPADE) && dura >= 132)
			return true;
		if(item.getType().equals(Material.WOOD_SPADE) && dura >= 60)
			return true;

		return false;
    }
}
