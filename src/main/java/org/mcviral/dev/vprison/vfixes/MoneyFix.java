package org.mcviral.dev.vprison.vfixes;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.mcviral.dev.vprison.vPrison;

public class MoneyFix {
	public static MoneyFix moneyfix;
	@SuppressWarnings("unused")
	private vPrison plugin;
    public HashMap<Player, Inventory> cstAnvil = new HashMap<Player, Inventory>();
	
	public MoneyFix(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public String convertBal(Player player) {
//		return coolFormat(plugin.econ.getBalance(player),0);
		if(vPrison.econ.getBalance(player) > 0)
			return formatValue(vPrison.econ.getBalance(player));
		return "0";
	}
	
	public static String formatValue(double value) {
		int power; 
		    String suffix = " kmbt";
		    String formattedNumber = "";

		    NumberFormat formatter = new DecimalFormat("#,###.#");
		    power = (int)StrictMath.log10(value);
		    value = value/(Math.pow(10,(power/3)*3));
		    formattedNumber=formatter.format(value);
		    formattedNumber = formattedNumber + suffix.charAt(power/3);
		    return formattedNumber.length()>4 ?  formattedNumber.replaceAll("\\.[0-9]+", "") : formattedNumber;  
		}
}
