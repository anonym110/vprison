package org.mcviral.dev.vprison.vfixes;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.mcviral.dev.vprison.vPrison;

public class NoHunger {
	public static NoHunger nohunger;
	@SuppressWarnings("unused")
	private vPrison plugin;
	
	public NoHunger(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void HungerEvent(FoodLevelChangeEvent event) {
		Player player = (Player) event.getEntity();
		if(plugin.perms.playerHas(player, "vprison.nohunger"))
			event.setCancelled(true);
	}

}
