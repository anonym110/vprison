package org.mcviral.dev.vprison.vfixes;


import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Utilities;

public class FishingDrops {
	public static FishingDrops fishingdrops;
	private vPrison plugin;
	
	public FishingDrops(vPrison plugin) {
		this.plugin = plugin;
	}

    public void onPlayerFish(PlayerFishEvent event){
        if(event.getCaught() instanceof Item){   	
            Item item = (Item) event.getCaught();
            
            ItemStack fish = new ItemStack(Material.RAW_FISH);
            int rnd = Utilities.randomWithRange(1, 100);
            
            if(rnd <= 60)
            	fish.setDurability((short) 0);
            else if(rnd > 60 && rnd <= 85)
            	fish.setDurability((short) 1);
            else if(rnd > 85 && rnd <= 87)
            	fish.setDurability((short) 2);
            else if(rnd > 87 && rnd <= 100)
            	fish.setDurability((short) 3);
            
            item.setItemStack(fish);
        }
    } 
}
