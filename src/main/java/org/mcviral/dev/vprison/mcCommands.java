package org.mcviral.dev.vprison;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.mcviral.dev.vprison.time.LoginReward;
import org.mcviral.dev.vprison.vchat.Announce;
import org.mcviral.dev.vprison.vcrates.CratesCmd;
import org.mcviral.dev.vprison.vevents.HourlyWar;
import org.mcviral.dev.vprison.vfarmlimit.FarmlimitCmd;
import org.mcviral.dev.vprison.vitems.ItemsCmd;
import org.mcviral.dev.vprison.vprestige.PrestigeCmd;
import org.mcviral.dev.vprison.vpvp.PVPRewards;

public class mcCommands implements CommandExecutor {
	@SuppressWarnings("unused")
	private vPrison plugin;
	 
	public mcCommands(vPrison plugin) {
		this.plugin = plugin;
	}
 
//	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("vitem")) {
			ItemsCmd.itemscmd.vitem(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("percent")) {
			ItemsCmd.itemscmd.percent(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("upgrade")) {
			ItemsCmd.itemscmd.upgrade(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("rankup")) {
			PrestigeCmd.prestigecmd.rankup(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("prestige")) {
			PrestigeCmd.prestigecmd.prestige(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("farmlimit")) {
			FarmlimitCmd.farmlimitcmd.farmlimit(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("ticket")) {
			CratesCmd.cratescmd.ticket(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("vbroad")) {
			Announce.announce.broadcast(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("war")) {
			HourlyWar.hourlywar.cmd(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("addvotekey")) {
			CratesCmd.cratescmd.addVoteKey(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("addvotecrate")) {
			CratesCmd.cratescmd.addVoteCrate(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("pvpreward")) {
			PVPRewards.pvprewards.Command(sender, cmd, label, args);
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("achievement")) {
			LoginReward.loginreward.Cmd(sender, cmd, label, args);
			return true;
		}
		return false;
	}
}