package org.mcviral.dev.vprison.vcrates;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;

public class CratesListener {
	@SuppressWarnings("unused")
	private vPrison plugin;
	public static CratesListener crateslistener;

	
	public CratesListener(vPrison plugin) {
		this.plugin = plugin;
	}
	
    public void onCrateOpen(InventoryClickEvent event){
    	if (event.getCurrentItem() != null) {
	    	ItemStack crate = event.getCurrentItem();
	    	Inventory inv = event.getWhoClicked().getInventory();
	    	if(event.getClick().isRightClick() && isMysteryCrate(crate) && !vCrates.vcrates.isActive) {
	    		event.setCancelled(true);
	    		if(inv.containsAtLeast(getKey(crate), 1)) {
	    			inv.removeItem(getKey(crate));
	    			vCrates.vcrates.CrateOpen((Player) event.getWhoClicked(), Crate(crate));
	    		} else {
	    			event.getWhoClicked().sendMessage("You dont have a fitting key to open this Box!");
	    		}
	    	}	
    	}
    }
    
    
	private ItemStack getKey(ItemStack crate) {
		if (crate != null) {
			if(crate.isSimilar(vCrates.vcrates.Tier2))
				return vCrates.vcrates.Tier2Key;
			if(crate.isSimilar(vCrates.vcrates.Tier1))
				return vCrates.vcrates.Tier1Key;
			if(crate.isSimilar(vCrates.vcrates.Vote))
				return vCrates.vcrates.VoteKey;		
		}
		return null;
	}


	private boolean isMysteryCrate(ItemStack crate) {
		if (crate != null) {
			if(crate.isSimilar(vCrates.vcrates.Tier2))
				return true;
			if(crate.isSimilar(vCrates.vcrates.Tier1))
				return true;
			if(crate.isSimilar(vCrates.vcrates.Vote))
				return true;
		}
		return false;
	}
	
	private ItemStack Crate(ItemStack crate) {
		if (crate != null) {
			if(crate.isSimilar(vCrates.vcrates.Tier2))
				return vCrates.vcrates.Tier1;
			if(crate.isSimilar(vCrates.vcrates.Tier1))
				return vCrates.vcrates.Tier1;
			if(crate.isSimilar(vCrates.vcrates.Vote))
				return vCrates.vcrates.Vote;
		}
		return null;
	}


	public void onBoxClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory customInv = vCrates.vcrates.cstInv.get(player);
		if(event.getInventory().equals(customInv) && event.getCurrentItem() != null) {				
				event.setCancelled(true);	
		}
	}	
    
}

