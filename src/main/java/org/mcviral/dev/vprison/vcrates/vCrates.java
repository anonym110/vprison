package org.mcviral.dev.vprison.vcrates;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Custom;
import org.mcviral.dev.vprison.utilities.RandomCollection;

public class vCrates {
	public static vCrates vcrates;
	private vPrison plugin;
	public HashMap<Player, Inventory> cstInv = new HashMap<Player, Inventory>();
	boolean isActive = false;
	public ItemStack Tier1 = Custom.Item(ChatColor.LIGHT_PURPLE + "MysteryCrate - Tier 1", new ItemStack(Material.PAPER), Custom.lore("Right Click to open"));
	public ItemStack Tier1Key = Custom.Item(ChatColor.LIGHT_PURPLE + "MysteryKey - Tier 1", new ItemStack(Material.TRIPWIRE_HOOK), Custom.lore("Needed to open the Tier1 Crate"));
	public ItemStack Tier2 = Custom.Item(ChatColor.DARK_PURPLE + "MysteryCrate - Tier 2", new ItemStack(Material.PAPER), Custom.lore("Right Click to open"));
	public ItemStack Tier2Key = Custom.Item(ChatColor.DARK_PURPLE + "MysteryKey - Tier 2", new ItemStack(Material.TRIPWIRE_HOOK), Custom.lore("Needed to open the Tier2 Crate"));
	public ItemStack Vote = Custom.Item(ChatColor.GREEN + "VoteCrate", new ItemStack(Material.PAPER), Custom.lore("Right Click to open"));
	public ItemStack VoteKey = Custom.Item(ChatColor.GREEN + "VoteKey", new ItemStack(Material.TRIPWIRE_HOOK), Custom.lore("Needed to open the VoteCrate"));

	
	public vCrates(vPrison plugin) {
		this.plugin = plugin;
	}
		
	public void CrateOpen(Player player, ItemStack boxtype) {
		if(isActive)
			return;
		isActive = true;
		
		player.getInventory().removeItem(boxtype);

//	   	if (player.getInventory().containsAtLeast(boxtype, 2))
//	   		player.getInventory().getItem(player.getInventory().first(boxtype)).setAmount(player.getInventory().getItem(player.getInventory().first(boxtype)).getAmount() - 1);
//	   	else
//	   		player.getInventory().removeItem(boxtype);

		cstInv.put(player, plugin.getServer().createInventory(player, InventoryType.CHEST, boxtype.getItemMeta().getDisplayName()));
		Inventory customInv = cstInv.get(player);
		ItemStack placeholder = new ItemStack(Material.STAINED_GLASS_PANE,1);
		placeholder.setDurability((short) 15);
		ItemMeta pMeta = placeholder.getItemMeta();
		pMeta.setDisplayName(ChatColor.BLACK + "x");
		placeholder.setItemMeta(pMeta);
		for(int i=0; i < 9; i++)
			customInv.setItem(i, placeholder);
		for(int i=18; i < 27; i++)
			customInv.setItem(i, placeholder);
		placeholder.setDurability((short) 4);
		customInv.setItem(4, placeholder);
		customInv.setItem(22, placeholder);
		player.openInventory(customInv);

		final Inventory customfinalInv = customInv;
		final ItemStack box = boxtype;
		final Player p = player;
		
		final int taskID = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
  		  public void run() {
  			  move(customfinalInv, box);
  		  }
  		}, 5L, 2L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Bukkit.getScheduler().cancelTask(taskID);
				final int taskID = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			  		  public void run() {
			  			  move(customfinalInv, box);
			  		  }
			  	}, 2L, 3L);
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Bukkit.getScheduler().cancelTask(taskID);
						final int taskID = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
					  		  public void run() {
					  			  move(customfinalInv, box);
					  		  }
					  	}, 3L, 4L);
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								Bukkit.getScheduler().cancelTask(taskID);
								ItemStack item = customfinalInv.getItem(13);
								String name = item.getType().toString();
								if(item.getItemMeta().hasDisplayName())
									name = item.getItemMeta().getDisplayName();
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&cCrates&f] &7You won " + name));
								p.getInventory().addItem(customfinalInv.getItem(13));
								isActive = false;
							}
						}, 30L);
					}
				}, 40L);
			}
		}, 60L);
	}

	public void move(Inventory customInv, ItemStack tier) {
		for (int i = 17; i >= 10; i--) {
			if (customInv.getItem(i-1) != null)
				customInv.setItem(i, customInv.getItem(i-1));
			else
				customInv.clear(i);
		}
		if (tier.isSimilar(Tier1))
			customInv.setItem(9, tier1prize());
		if (tier.isSimilar(Vote))
			customInv.setItem(9, voteprize());

	}
		
	public ItemStack tier1prize() {	
		RandomCollection<ItemStack> items = new RandomCollection<ItemStack>();
		
    	items.add(5.0, Custom.Item(ChatColor.RED + "D-Sword", new ItemStack(Material.DIAMOND_SWORD), Custom.lore("Upgradeable"))); 
    	items.add(15.0, Custom.Item(ChatColor.GREEN + "D-Helmet", new ItemStack(Material.DIAMOND_HELMET), Custom.lore("Upgradeable"))); 
    	items.add(0.1, Custom.Item(ChatColor.RED + "PAYDAY", new ItemStack(Material.EMERALD, 15), Custom.lore("moneyz"))); 
    	items.add(10.0, Custom.Item(ChatColor.RED + "Gold Ingot", new ItemStack(Material.GOLD_INGOT, 12), Custom.lore("Upgradeable"))); 
    	items.add(1.0, Custom.Item(ChatColor.MAGIC + "Golden Apple", new ItemStack(Material.GOLDEN_APPLE, 5), Custom.lore("Upgradeable"))); 
    	
		return items.next();
	}
	
	public ItemStack voteprize() {	
		RandomCollection<ItemStack> items = new RandomCollection<ItemStack>();
		
    	items.add(100.0, new ItemStack(Material.COOKED_BEEF, 16));   	
    	items.add(100.0, new ItemStack(Material.APPLE, 32));  
    	items.add(80.0, new ItemStack(Material.COOKED_BEEF, 32));   	
    	
    	items.add(40.0, new ItemStack(Material.DIAMOND, 16));  
    	items.add(60.0, new ItemStack(Material.IRON_INGOT, 16));  
    	items.add(50.0, new ItemStack(Material.GOLD_INGOT, 16));
    	items.add(70.0, new ItemStack(Material.COAL, 16));
    	
    	items.add(25.0, new ItemStack(Material.GOLDEN_APPLE, 5));  
    	
    	items.add(20.0, new ItemStack(Material.DIAMOND_PICKAXE, 1));  
    	items.add(25.0, new ItemStack(Material.DIAMOND_AXE, 1));  
    	items.add(25.0, new ItemStack(Material.DIAMOND_SPADE, 1));  
    	
    	items.add(5.0, Custom.Item(ChatColor.AQUA + "Iron Sword", new ItemStack(Material.IRON_SWORD), Custom.lore(ChatColor.GRAY + "Upgradeable"))); 
    	items.add(9.0, Custom.Item(ChatColor.AQUA + "Iron Helmet", new ItemStack(Material.IRON_HELMET), Custom.lore(ChatColor.GRAY + "Upgradeable"))); 
    	items.add(6.0, Custom.Item(ChatColor.AQUA + "Iron Chestplate", new ItemStack(Material.IRON_CHESTPLATE), Custom.lore(ChatColor.GRAY + "Upgradeable"))); 
    	items.add(7.0, Custom.Item(ChatColor.AQUA + "Iron Leggings", new ItemStack(Material.IRON_LEGGINGS), Custom.lore(ChatColor.GRAY + "Upgradeable"))); 
    	items.add(10.0, Custom.Item(ChatColor.AQUA + "Iron Boots", new ItemStack(Material.IRON_BOOTS), Custom.lore(ChatColor.GRAY + "Upgradeable"))); 
    	
    	ItemStack jackpot = Custom.Item(ChatColor.GREEN + "Voters Speed", new ItemStack(Material.DIAMOND_PICKAXE), Custom.lore(ChatColor.GRAY + ""));
    	jackpot.addUnsafeEnchantment(Enchantment.DIG_SPEED, 5);
    	items.add(0.5, jackpot);
    	
    	ItemStack jackpot2 = Custom.Item(ChatColor.GREEN + "Voters Wrath", new ItemStack(Material.DIAMOND_SWORD), Custom.lore(ChatColor.GRAY + "Upgradeable"));
    	jackpot.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
    	items.add(0.1, jackpot2);
    	
		return items.next();
	}
	
	public void addCrate(Player player, int amount) {
		for(int i = 0; i < amount; i++) {
			player.getInventory().addItem(Tier1);
			player.getInventory().addItem(Tier1Key);
			player.getInventory().addItem(Vote);
			player.getInventory().addItem(VoteKey);
		}
	}
	
	public void addVoteCrate(Player player, int amount) {
		for(int i = 0; i < amount; i++) {
			player.getInventory().addItem(Vote);
		}
	}
	
	public void addVoteKey(Player player, int amount) {
		for(int i = 0; i < amount; i++) {
			player.getInventory().addItem(VoteKey);
		}
	}

}
