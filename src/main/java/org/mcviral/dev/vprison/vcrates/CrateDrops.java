package org.mcviral.dev.vprison.vcrates;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Utilities;

public class CrateDrops {
	public static CrateDrops cratedrops;
	@SuppressWarnings("unused")
	private vPrison plugin;
	
	public CrateDrops(vPrison plugin) {
		this.plugin = plugin;
	}
	
    public void onBlockBreak(BlockBreakEvent event){
    	Player player = event.getPlayer();
    	if(!event.isCancelled() && player.getGameMode() != GameMode.CREATIVE) {
    		int rnd = Utilities.randomWithRange(1, 1000000);
//    		int rnd = Utilities.randomWithRange(1, 300);
//    		plugin.getLogger().info("RANDOM: " + rnd);
    		if(rnd >= 1 && rnd <= 25) {
    			if(player.getInventory().firstEmpty() == -1)
    				event.getBlock().getLocation().getWorld().dropItemNaturally(event.getBlock().getLocation(), vCrates.vcrates.Vote);
    			else			
    				player.getInventory().addItem(vCrates.vcrates.Vote);
    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&kX&b Congratulations! You found a &aVoteCrate&b &a&kX"));
    		}
    		if(rnd >= 100 && rnd <= 110) {
    			if(player.getInventory().firstEmpty() == -1)
    				event.getBlock().getLocation().getWorld().dropItemNaturally(event.getBlock().getLocation(), vCrates.vcrates.Tier1);
    			else			
    				player.getInventory().addItem(vCrates.vcrates.Tier1);
    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&kX&b Congratulations! You found a &aTier 1 - MysteryCrate&b &a&kX"));
    		}
    		if(rnd >= 200 && rnd <= 205) {
    			if(player.getInventory().firstEmpty() == -1)
    				event.getBlock().getLocation().getWorld().dropItemNaturally(event.getBlock().getLocation(), vCrates.vcrates.Tier2);
    			else			
    				player.getInventory().addItem(vCrates.vcrates.Tier2);
    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&kX&b Congratulations! You found a &aTier 2 - MysteryCrate&b &a&kX"));
    		}
    		
    		
    		
    		if(player.getWorld().getName().equalsIgnoreCase("world") && !event.getBlock().getType().equals(Material.SNOW_BLOCK)) {
	    		if(rnd >= 10000 && rnd <= 15500) {
	    			player.removePotionEffect(PotionEffectType.FAST_DIGGING);
	    			player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 20*30, 1));
	    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bOh wow! You recieved a Mining Boost for destroying this Block..."));
	    		}
    		}
    	}
    } 
	
}
