package org.mcviral.dev.vprison.vcrates;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcviral.dev.vprison.vPrison;

public class CratesCmd {
	private vPrison plugin;
	public static CratesCmd cratescmd;
	
	public CratesCmd(vPrison plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public void ticket(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length > 2) {

		} else {	
//			plugin.getLogger().info("1337: " + args[0] + args[1]);
			if(args[0].equalsIgnoreCase("votekey")) {
				plugin.getLogger().info("1337: ");
				Player target = plugin.getServer().getPlayer(args[1]);
				target.getInventory().addItem(vCrates.vcrates.VoteKey);
			} else if(args[0].equalsIgnoreCase("votecrate")) {
				Player target = plugin.getServer().getPlayer(args[1]);
				target.getInventory().addItem(vCrates.vcrates.Vote);
			} else if(args[0].equalsIgnoreCase("tier1key")) {
				Player target = plugin.getServer().getPlayer(args[1]);
				target.getInventory().addItem(vCrates.vcrates.Tier1Key);
			} else if(args[0].equalsIgnoreCase("tier1crate")) {
				Player target = plugin.getServer().getPlayer(args[1]);
				target.getInventory().addItem(vCrates.vcrates.Tier1);
			} else if(args[0].equalsIgnoreCase("tier2key")) {
				Player target = plugin.getServer().getPlayer(args[1]);
				target.getInventory().addItem(vCrates.vcrates.Tier2Key);
			} else if(args[0].equalsIgnoreCase("tier2crate")) {
				Player target = plugin.getServer().getPlayer(args[1]);
				target.getInventory().addItem(vCrates.vcrates.Tier2);
			}
		}
	}
	
	
	@SuppressWarnings("deprecation")
	public void addVoteCrate(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0 && sender instanceof Player) {
			Player player = (Player) sender;
			vCrates.vcrates.addVoteCrate(player, 1);
		}
		else if(args.length == 1)
			vCrates.vcrates.addVoteCrate(plugin.getServer().getPlayer(args[0]), 1);
		else
			vCrates.vcrates.addVoteCrate(plugin.getServer().getPlayer(args[0]), Integer.parseInt(args[1]));
	}
	
	@SuppressWarnings("deprecation")
	public void addVoteKey(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0 && sender instanceof Player) {
			Player player = (Player) sender;
			vCrates.vcrates.addVoteKey(player, 1);
		}
		else if(args.length == 1)
			vCrates.vcrates.addVoteKey(plugin.getServer().getPlayer(args[0]), 1);
		else
			vCrates.vcrates.addVoteKey(plugin.getServer().getPlayer(args[0]), Integer.parseInt(args[1]));
	}
	
}
