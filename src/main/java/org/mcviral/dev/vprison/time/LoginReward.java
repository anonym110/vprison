package org.mcviral.dev.vprison.time;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Custom;
import org.mcviral.dev.vprison.utilities.Files;

public class LoginReward {
	private vPrison plugin;
	public static LoginReward loginreward;
	public HashMap<Player, Inventory> cstInv = new HashMap<Player, Inventory>();
	Files rewardyml = new Files("milestones.yml");  
	String PluginTag = ChatColor.translateAlternateColorCodes('&', "&f[&eMilestones&f]&7 ");
	final int DAY = 86400000;
	final int HOUR = 3600000;
	
	public LoginReward(vPrison plugin) {
		this.plugin = plugin;
	}
	
	public void OpenInterface(Player player) {
		cstInv.put(player, plugin.getServer().createInventory(player, 9, ChatColor.DARK_PURPLE + "Welcome " + player.getName() + "!"));
		final Inventory customInv = cstInv.get(player);
		
		String[] text1 = {
				ChatColor.GRAY + "[24th April] " + ChatColor.DARK_GRAY + "Now implemented Milestone Rewards!",
				ChatColor.GRAY + "[24th April] " + ChatColor.DARK_GRAY + "Fixed War.. except for the failing to tp :P"};	
		customInv.setItem(0, Custom.Item(ChatColor.WHITE + "" + ChatColor.UNDERLINE + "News", new ItemStack(Material.PAPER), Custom.lore(text1)));
		
		customInv.setItem(7, MineReward(player));

		customInv.setItem(8, TimeReward(player));


	    player.openInventory(customInv);

	}
	
	public void LoginEvent(final PlayerJoinEvent event) {
	    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	        @Override
	        public void run() {
	    		OpenInterface(event.getPlayer());
	        }
	    }, 10L);
	}
	
	public void Cmd(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player)) {
			Player player = (Player) sender;
			OpenInterface(player);
		} else {
			sender.sendMessage(ChatColor.RED + "This command can only be run by a player.");
		}
	}
	
	public void InterfaceClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory customInv = cstInv.get(player);
		
		if(event.getInventory().equals(customInv)) {
			event.setCancelled(true);
			
			if(event.getRawSlot() == 7)
				MineClick(player, event);
			else if(event.getRawSlot() == 8) 
				TimeClick(player, event);
		}
			
	}
	
	public ItemStack MineReward(Player player) {
		long blocks = Milestones.milestones.getBlocksMined(player);
		int level = 0;
		if(rewardyml.getConfig().contains(player.getUniqueId() + ".BlocksReward"))
			level = rewardyml.getConfig().getInt(player.getUniqueId() + ".BlocksReward");
		ItemStack item = Custom.Item(ChatColor.RED + "Error", new ItemStack(Material.REDSTONE), Custom.lore("Something went wrong :o"));;
		
		if(blocks >= 2500000 && level >= 5) {
			if(level != 6) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Blocks mined: " + ChatColor.YELLOW + blocks, ChatColor.GRAY + "No more rewards currently added."};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(blocks >= 500000 && level >= 4) {
			if(level != 5) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Blocks mined: " + ChatColor.YELLOW + blocks, ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"2,500,000 " + ChatColor.GRAY + "Blocks"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(blocks >= 100000 && level >= 3) {
			if(level != 4) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Blocks mined: " + ChatColor.YELLOW + blocks, ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"500,000 " + ChatColor.GRAY + "Blocks"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(blocks >= 25000 && level >= 2) {
			if(level != 3) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Blocks mined: " + ChatColor.YELLOW + blocks, ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"100,000 " + ChatColor.GRAY + "Blocks"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(blocks >= 5000 && level >= 1) {
			if(level != 2) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Blocks mined: " + ChatColor.YELLOW + blocks, ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"25,000 " + ChatColor.GRAY + "Blocks"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(blocks >= 1000) {		
			if(level != 1) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Blocks mined: " + ChatColor.YELLOW + blocks, ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"5,000 " + ChatColor.GRAY + "Blocks"};	
				item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else {
			String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Blocks mined: " + ChatColor.YELLOW + blocks, ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"1,000 " + ChatColor.GRAY + "Blocks"};	
			item = Custom.Item(ChatColor.GOLD + "Mining Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
		}
		return item;
	}
	
	public ItemStack TimeReward(Player player) {
		long time = Milestones.milestones.getOnlineTime(player);
		int minutes = (int) ((time / (1000*60)) % 60);
		int hours   = (int) ((time / (1000*60*60)) % 24);
		int level = 0;
		ItemStack item = Custom.Item(ChatColor.RED + "Error", new ItemStack(Material.IRON_FENCE), Custom.lore("Something went wrong :o"));;
		if(rewardyml.getConfig().contains(player.getUniqueId() + ".TimeReward"))
			level = rewardyml.getConfig().getInt(player.getUniqueId() + ".TimeReward");

		if(time >= DAY * 21 && level >= 4) {
			if(level != 5) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Time online: " + ChatColor.YELLOW + hours + "h " + minutes + "m", ChatColor.GRAY + "No more rewards currently added."};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(time >= DAY * 7 && level >= 3) {
			if(level != 4) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Time online: " + ChatColor.YELLOW + hours + "h " + minutes + "m", ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"504h" + ChatColor.GRAY + "."};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(time >= DAY * 3 && level >= 2) {
			if(level != 3) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Time online: " + ChatColor.YELLOW + hours + "h " + minutes + "m", ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"168h" + ChatColor.GRAY + "."};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(time >= DAY && level >= 1) {
			if(level != 2) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Time online: " + ChatColor.YELLOW + hours + "h " + minutes + "m", ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"72h" + ChatColor.GRAY + "."};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else if(time >= HOUR * 5) {		
			if(level != 1) {
				String[] text3 = {ChatColor.GREEN + "[unlocked]", ChatColor.GRAY + "Right click to claim reward!"};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.DIAMOND), Custom.lore(text3));
			} else {
				String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Time online: " + ChatColor.YELLOW + hours + "h " + minutes + "m", ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"24h" + ChatColor.GRAY + "."};	
				item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
			}
		} else {
			String[] text3 = {ChatColor.RED + "[locked]", ChatColor.GRAY + "Time online: " + ChatColor.YELLOW + hours + "h " + minutes + "m", ChatColor.GRAY + "Next Reward at " + ChatColor.YELLOW +"5h" + ChatColor.GRAY + "."};	
			item = Custom.Item(ChatColor.GOLD + "Online Time Milestone", new ItemStack(Material.IRON_FENCE), Custom.lore(text3));
		}		
		return item;
	}
	
	public void MineClick(Player player, InventoryClickEvent event) {
		long blocks = Milestones.milestones.getBlocksMined(player);
		int level;
		if(rewardyml.getConfig().contains(player.getUniqueId() + ".BlocksReward"))
			level = rewardyml.getConfig().getInt(player.getUniqueId() + ".BlocksReward");
		else 
			level = 0;
		
		if(blocks >= 2500000 && level >= 5) {
			if(level != 6) {
				level++;
				addLevel(player.getUniqueId() + ".BlocksReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "BlocksPrize#6", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(blocks >= 500000 && level >= 4) {
			if(level != 5) {
				level++;
				addLevel(player.getUniqueId() + ".BlocksReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "BlocksPrize#5", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(blocks >= 100000 && level >= 3) {
			if(level != 4) {
				level++;
				addLevel(player.getUniqueId() + ".BlocksReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "BlocksPrize#4", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(blocks >= 25000 && level >= 2) {
			if(level != 3) {
				level++;
				addLevel(player.getUniqueId() + ".BlocksReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "BlocksPrize#3", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(blocks >= 5000 && level >= 1) {
			if(level != 2) {
				level++;
				addLevel(player.getUniqueId() + ".BlocksReward");
				String[] text = {ChatColor.WHITE + "Hard work pays off ;)"};
				ItemStack item = Custom.Item(ChatColor.RED + "", new ItemStack(Material.IRON_PICKAXE));
				item.addUnsafeEnchantment(Enchantment.DIG_SPEED, 2);
				reward(Custom.Item(ChatColor.GREEN + "Workers Pick", item, Custom.lore(text)),player);
				player.sendMessage(PluginTag + ChatColor.GRAY + "Rewarded with " + ChatColor.GREEN + "Workers Pick");
			}
		} else if(blocks >= 1000) {		
			if(level != 1) {
				level++;
				addLevel(player.getUniqueId() + ".BlocksReward");
				reward(new ItemStack(Material.DIAMOND, 5),player);
				player.sendMessage(PluginTag + ChatColor.GRAY + "Rewarded with " + ChatColor.AQUA + "5 Diamonds");
			}
		} else {
			//not reached first Milestone
		}
		cstInv.get(player).setItem(7, MineReward(player));
	}
	
	public void TimeClick(Player player, InventoryClickEvent event) {
		long time = Milestones.milestones.getOnlineTime(player);
		int level = 0;
		if(rewardyml.getConfig().contains(player.getUniqueId() + ".TimeReward"))
			level = rewardyml.getConfig().getInt(player.getUniqueId() + ".TimeReward");
		
		if(time >= DAY * 21 && level >= 4) {
			if(level != 5) {
				level++;
				addLevel(player.getUniqueId() + ".TimeReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "TimePrize#5", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(time >= DAY * 7 && level >= 3) {
			if(level != 4) {
				level++;
				addLevel(player.getUniqueId() + ".TimeReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "TimePrize#4", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(time >= DAY * 3 && level >= 2) {
			if(level != 3) {
				level++;
				addLevel(player.getUniqueId() + ".TimeReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "TimePrize#3", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(time >= DAY && level >= 1) {
			if(level != 2) {
				level++;
				addLevel(player.getUniqueId() + ".TimeReward");
				String[] text = {ChatColor.GRAY + "Reward hasnt been determined yet", ChatColor.GRAY + "Ask " + ChatColor.YELLOW + "anonym110" + ChatColor.GRAY + " to trade for a reward."};
				reward(Custom.Item(ChatColor.RED + "TimePrize#2", new ItemStack(Material.PAPER), Custom.lore(text)),player);
			}
		} else if(time >= HOUR * 5) {		
			if(level != 1) {
				level++;
				addLevel(player.getUniqueId() + ".TimeReward");
				reward(new ItemStack(Material.DIAMOND, 5),player);
				player.sendMessage(PluginTag + ChatColor.GRAY + "Rewarded with " + ChatColor.AQUA + "5 Diamonds");
			}
		} else {
			//
		}
		cstInv.get(player).setItem(8, TimeReward(player));
	}
		
	public void addLevel(String path) {
		int level = 0;
		if(rewardyml.getConfig().contains(path))
			level = rewardyml.getConfig().getInt(path);
		level++;
		rewardyml.getConfig().set(path, level);
		rewardyml.saveConfig();
	}

	public void reward(ItemStack item, Player player) {
		if(player.getInventory().firstEmpty() == -1)
			player.getLocation().getWorld().dropItemNaturally(player.getLocation(), item);
		else
			player.getInventory().addItem(item);
	}

}
