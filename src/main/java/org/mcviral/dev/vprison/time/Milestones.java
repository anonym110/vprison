package org.mcviral.dev.vprison.time;

import java.util.HashMap;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.mcviral.dev.vprison.vPrison;
import org.mcviral.dev.vprison.utilities.Files;

public class Milestones {
	private vPrison plugin;
	public static Milestones milestones;
    HashMap<Player, Long> online = new HashMap<Player, Long>();
//    HashMap<Player, Integer> days = new HashMap<Player, Integer>();
    HashMap<Player, Long> login = new HashMap<Player, Long>();
	Files milestonesyml = new Files("loginstats.yml");  
	
	public Milestones(vPrison plugin) {
		this.plugin = plugin;
	}

	public long getOnlineTime(Player player) {
		updateTime(player);
		if(online.containsKey(player))
			return online.get(player);
		else
			if(milestonesyml.getConfig().contains(player.getUniqueId() + ".onlineTime"))
				return milestonesyml.getConfig().getLong(player.getUniqueId() + ".onlineTime");
			else
				return 0;
	}
	
	public int getBlocksMined(Player player) {
		if(milestonesyml.getConfig().contains(player.getUniqueId() + ".brokenBlocks"))
			return milestonesyml.getConfig().getInt(player.getUniqueId() + ".brokenBlocks");
		else
			return 0;
	}
	
//	public int getDaysPlayed(Player player) {
//		if(days.containsKey(player))
//			return days.get(player);
//		return 0;
//	}
	
	public void setOnlineTime(Player player, long amount) {
		online.put(player, amount);
	}
	
	public void setBlocksMined(Player player, int amount) {
		milestonesyml.getConfig().set(player.getUniqueId() + ".brokenBlocks", amount);
		milestonesyml.saveConfig();
	}
	
//	public void setDaysPlayed(Player player, int amount) {
//		days.put(player, amount);
//	}

	public void login(PlayerJoinEvent event) {	
		Player player = event.getPlayer();
		if(milestonesyml.getConfig().contains(player.getUniqueId() + ".brokenBlocks")) {
			long onlineTime = milestonesyml.getConfig().getLong(player.getUniqueId() + ".onlineTime");
			online.put(player, onlineTime);
		} else {
			online.put(player, (long) 0);
		}
		login.put(player, System.currentTimeMillis());
	}
	
	public void updateTime(Player player) {
		long timeOnline = System.currentTimeMillis() - login.get(player);
		long newTime = online.get(player) + timeOnline;
		milestonesyml.getConfig().set(player.getUniqueId() + ".onlineTime", newTime);
		milestonesyml.saveConfig();
		online.put(player, newTime);
		login.put(player, System.currentTimeMillis());
	}

	public void logout(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		long timeOnline = System.currentTimeMillis() - login.get(player);
		long newTime = online.get(player) + timeOnline;
		milestonesyml.getConfig().set(player.getUniqueId() + ".onlineTime", newTime);
		milestonesyml.saveConfig();
		online.remove(player);
		login.remove(player);
	}

	public void shutdown() {
    	for(Player player : plugin.getServer().getOnlinePlayers()) {
    		long timeOnline = System.currentTimeMillis() - login.get(player);
    		long newTime = online.get(player) + timeOnline;
    		milestonesyml.getConfig().set(player.getUniqueId() + ".onlineTime", newTime);
    		milestonesyml.saveConfig();
    		online.remove(player);
    		login.remove(player);
    	}
	}
	
	public void blockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if(player.getGameMode() == GameMode.SURVIVAL && !event.isCancelled()) {
			int brokenBlocks = 0;
			if(milestonesyml.getConfig().contains(player.getUniqueId() + ".brokenBlocks"))
				brokenBlocks = milestonesyml.getConfig().getInt(player.getUniqueId() + ".brokenBlocks");
			brokenBlocks++;
			milestonesyml.getConfig().set(player.getUniqueId() + ".brokenBlocks", brokenBlocks);
			milestonesyml.saveConfig();
		}
	}
	
	public void newDay() {
		//is new day? compare to saved last day
		//add day
		//save day
	}

}
