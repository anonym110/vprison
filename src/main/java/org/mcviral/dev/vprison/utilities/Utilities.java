package org.mcviral.dev.vprison.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.mcviral.dev.vprison.vPrison;


public class Utilities {
	public static Utilities utilities;
	@SuppressWarnings("unused")
	private vPrison plugin;
	 	
	public Utilities(vPrison plugin) {
		this.plugin = plugin;
	}
	

    	public static void save(Object obj,String path) throws Exception
    	{
    		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
    		oos.writeObject(obj);
    		oos.flush();
    		oos.close();
    	}
    	public static Object load(String path) throws Exception
    	{
    		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
    		Object result = ois.readObject();
    		ois.close();
    		return result;
    	}
    	
    	public static int randomWithRange(int min, int max)
    	{
    	   int range = (max - min) + 1;     
    	   return (int)(Math.random() * range) + min;
    	}
    	
    	double randomWithRange(double min, double max)
    	{
    	   double range = (max - min);     
    	   return (Math.random() * range) + min;
    	}
    	
    	public static String IntegerToRomanNumeral(int input) {
    	    if (input < 1 || input > 3999)
    	        return "Invalid Roman Number Value";
    	    String s = "";
    	    while (input >= 1000) {
    	        s += "M";
    	        input -= 1000;        }
    	    while (input >= 900) {
    	        s += "CM";
    	        input -= 900;
    	    }
    	    while (input >= 500) {
    	        s += "D";
    	        input -= 500;
    	    }
    	    while (input >= 400) {
    	        s += "CD";
    	        input -= 400;
    	    }
    	    while (input >= 100) {
    	        s += "C";
    	        input -= 100;
    	    }
    	    while (input >= 90) {
    	        s += "XC";
    	        input -= 90;
    	    }
    	    while (input >= 50) {
    	        s += "L";
    	        input -= 50;
    	    }
    	    while (input >= 40) {
    	        s += "XL";
    	        input -= 40;
    	    }
    	    while (input >= 10) {
    	        s += "X";
    	        input -= 10;
    	    }
    	    while (input >= 9) {
    	        s += "IX";
    	        input -= 9;
    	    }
    	    while (input >= 5) {
    	        s += "V";
    	        input -= 5;
    	    }
    	    while (input >= 4) {
    	        s += "IV";
    	        input -= 4;
    	    }
    	    while (input >= 1) {
    	        s += "I";
    	        input -= 1;
    	    }    
    	    return s;
    	}
}
