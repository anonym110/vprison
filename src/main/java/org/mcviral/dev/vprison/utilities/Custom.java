package org.mcviral.dev.vprison.utilities;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Custom {
	
	public static ItemStack Item(String name, ItemStack item) {
		String desc = name;
		ItemStack is = item;
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(desc);
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack Item(String name, ItemStack item, List<String> lore) {
		String desc = name;
		ItemStack is = item;
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(desc);
		im.setLore(lore);
		is.setItemMeta(im);
		return is;
	}
	
	public static List<String> lore(String[] desc) {
		List<String> text = new ArrayList<String>();
		for(int i = 0; i < desc.length; i++) {
			text.add(desc[i]);
		}	
		return text;	
	}
	
	public static List<String> lore(String desc) {
		List<String> text = new ArrayList<String>();
		text.add(desc);
		return text;	
	}

}
