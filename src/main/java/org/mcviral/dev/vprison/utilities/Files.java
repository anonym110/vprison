package org.mcviral.dev.vprison.utilities;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.mcviral.dev.vprison.vPrison;

public class Files {
	private final String fileName;
	private vPrison plugin;
	private File configFile;
	private File pathfolder = new File("plugins/vPrison");
	private FileConfiguration fileConfiguration;

	public Files(String fileName) {
		this.fileName = fileName;
		this.configFile = new File("plugins/vPrison/" + fileName);
		create();
	}
	
	public void create() {		
		if (!configFile.exists()) {
			pathfolder.mkdirs();
			try {
				configFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	 
	}
	
	public void reloadConfig() {
		fileConfiguration = YamlConfiguration.loadConfiguration(configFile);

//		// Look for defaults in the jar
//		 InputStream defConfigStream = plugin.getResource(fileName);
//		 if (defConfigStream != null) {
//		 YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream)); 
//			fileConfiguration.setDefaults(defConfig);
//		}
	}

	public FileConfiguration getConfig() {
		if (fileConfiguration == null) {
			this.reloadConfig();
		}
		return fileConfiguration;
	}

	public void saveConfig() {
		if (fileConfiguration == null || configFile == null) {
			return;
		} else {
			try {
				getConfig().save(configFile);
			} catch (IOException ex) {
				plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
			}
		}
	}
	public void saveDefaultConfig() {
		if (!configFile.exists()) {
			this.plugin.saveResource(fileName, false);
		}
	} 
	 
}
